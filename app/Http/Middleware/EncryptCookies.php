<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        'cook_id_sala' //Se define la cookie que llegara al frontend y le pedimos que utilice el valor asignado sin encriptacion.
    ];
}
