<?php

namespace App\Http\Controllers;

use App\Models\Equipo;
use App\Models\Sala;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Cookie;

class SalasController extends Controller
{
    public static function create(Request $request)
    {
        $request->validate([
            'sala_nombre' => 'required|string|max:255|min:3',
            'sala_tiempoJuego' => 'required|integer|min:10|max:60',
            'sala_numJugadores' => 'required|integer|min:1|max:40',
            'sala_dificultad' => 'in:Facil,Media,Dificil'
        ]);

        $nueva_sala = Sala::crear($request);
         
        //TODO usar $nueva_sala == Null
        return view("sala_creada")->with("sala", $nueva_sala);
    }

    public function obtenerSalas()
    {
        return Sala::obtenerSalas();
    }

    public function join(Request $request)
    {
        $request->validate([
            'id_sala' => ['required', 'numeric', 'min:1']
        ]);

        $insertado = Equipo::ubicar_en_equipo($request);

        $sala = Sala::encontrar($request);

        if ($insertado) {
            return view("sala_creada")->with("sala", $sala);
        } else {
            return view('salas');
        }
        //TODO Mostrar en un cartel que no se le agrego a la partida porque ya no habia lugar en los equipos.
    }

    public function refrescarSala(Request $request)
    {
        return Sala::refrescarSala($request);
    }

    public function actualizarSala(Request $request)
    {
        $request->validate([
            'tiempo' => 'required|integer|min:10|max:60',
            'jugadores' => 'required|integer|min:1|max:40',
            'dificultad' => 'in:Facil,Media,Dificil'
        ]);

        return Sala::actualizar($request);
    }

    public function salirSala(Request $request)
    {
        return Sala::salirSala($request);
    }

    public function eliminarSala(Request $request)
    {
        return Sala::eliminar($request);
    }

    public function finalizarSala(Request $request)
    {
        try {
            $sala = Sala::where("id", $request->cookie('cook_id_sala'))->where("id_anfitrion", auth()->user()->id)->first();
            if (!isset($sala)) {
                return ["finalizada" => false];
            }
            return Sala::finalizarSala($request->cookie('cook_id_sala'));//TODO el metodo recibe un integer...
        } catch (Exception $e) {
            return ["finalizada" => false];
        }
    }

    /* */
    /* Codigo que a futuro, deberíamos separarlo a una "Partida" */
    /* */

    public function iniciar(Request $request)
    {
        $sala = Sala::iniciar($request);

        return view('vistaAnfitrion')->with("sala", $sala);
    }

    public function obtenerVistaJuego(Request $request)
    {
        if (Sala::verificarContinuidadSala($request->cookie('cook_id_sala'))) {
            return view('gameplay')->with("sala", Sala::find($request->cookie('cook_id_sala')));
        } else {
            return redirect()->route("salas");
        }
    }

    public function refrescarJuego(Request $request)
    {
        return Sala::refrescarJuego($request);
    }

    public function obtenerVistaFinJuego(Request $request)
    {
        $id_sala = $request->cookie('cook_id_sala');
        if (Sala::verificarContinuidadSala($id_sala)) {
            if (auth()->user()->id == Sala::find($id_sala)->id_anfitrion) {
                return view("finalAnfitrion")
                ->with("data", Sala::finJuegoAnfitrion($id_sala, auth()->user()->id))
                ->with("sala", Sala::find($request->cookie('cook_id_sala')));
            } else {
                return view("final")
                ->with("data", Sala::finJuego($id_sala, auth()->user()->id))
                ->with("sala", Sala::find($request->cookie('cook_id_sala')));
            }
        } else {
            return redirect()->route("salas");
        }
    }

    /* */
    /* Fin codigo que a futuro, deberíamos separarlo a una "Partida" */
    /* */
}
