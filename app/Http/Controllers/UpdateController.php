<?php

namespace App\Http\Controllers;

use App\Models\Equipo;
use Illuminate\Http\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class UpdateController extends Controller
{
    public function update()
    {
        $data['git'] = shell_exec( '(cd '. base_path() .' && git pull)' );
        if ($data["git"] != "Already up to date.\n"){
            $data['migration'] = shell_exec( '(cd '. base_path() .' && php artisan migrate:fresh --seed)' );
        } else
            $data['migration'] = "Not executed because of Already up to date.";

        return $data;
    }
}
