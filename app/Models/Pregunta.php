<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class Pregunta extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = "preguntas";

    protected $primaryKey = "id";

    public function respuestas()
    {
        return $this->hasMany(Respuesta::class);
    }

    public static function obtener_elementos_disponibles($id_sala)
    {
        $sala = Sala::find($id_sala);
        $elementos_disponibles = Pregunta::where("etapa", $sala->etapaActual)
            ->where("dificultad", $sala->dificultad)
            ->get();
        $result = [];
        foreach ($elementos_disponibles as $elemento_disponible) {
            if (AccionRealizada::verificar_timestamp(auth()->user()->id, $elemento_disponible->elemento_asociado, $sala->id)) {
                array_push($result, $elemento_disponible);
            }
        }
        return $result;
    }

    public static function verificar_etapa($elemento_asociado, $id_sala)
    {
        $elementos_disponibles = Self::obtener_elementos_disponibles($id_sala);
        foreach ($elementos_disponibles as $elemento_disponible) {
            if ($elemento_asociado == $elemento_disponible->elemento_asociado) {
                return true;
            }
        }
        return false;
    }

    public static function prepararRespuestas($respuestas)
    {
        $result_respuestas = [];

        for ($i=0; $i < count($respuestas); $i++) {
            $result_respuestas[$i] = array("id"=> $respuestas[$i]->id , "texto" => $respuestas[$i]->texto);
        }
        return $result_respuestas;
    }

    public static function getPregunta($userId, $elementoAsociado, $request)
    {
        /* */
        $response = Sala::refrescarJuego($request, false);
        /* */
        $idSala = $request->cookie('cook_id_sala');

        $respuestaValidacion = self::validarPeticion($userId, $idSala, $elementoAsociado, "pregunta");

        if (!$respuestaValidacion["valida"]) {
            /* */
            $response = Sala::refrescarJuego($request, false);
            /* */

            $response["valid_timestamp"] = false;
            $response["correcta"] = false;
            $response["justificacion"] = $respuestaValidacion["justificacion"];

            return json_encode($response);
        }

        $sala = Sala::find($idSala);

        $pregunta = self::where('elemento_asociado', $elementoAsociado)
            ->where('dificultad', $sala->dificultad)
            ->where("language", App::getLocale())
            ->first();

        $respuestas = self::prepararRespuestas($pregunta->respuestas);

        $response['valid_timestamp'] = true;
        $response['valid_etapa'] = true;
        $response['pregunta'] = $pregunta->enunciado;
        $response['respuestas'] = $respuestas;
        return json_encode($response);
    }

    public static function responder($request, $idUser)
    {
        $json_data = json_decode($request->getContent());
        $id_sala = $request->cookie('cook_id_sala');

        $respuestaValidacion = self::validarPeticion($idUser, $id_sala, $json_data->elemento_asociado, $json_data->respuesta);

        if (!$respuestaValidacion["valida"]) {
            /* */
            $response = Sala::refrescarJuego($id_sala, false);
            /* */
            $response["valid_timestamp"] = false;
            $response["correcta"] = false;
            $response["justificacion"] = $respuestaValidacion["justificacion"];
            
            return json_encode($response);
        }

        //Recupero los datos de la base y mando actualizar la temperatura y el bienestar
        $pregunta = Pregunta::where('elemento_asociado', $json_data->elemento_asociado)->where("language", App::getLocale())->first();
        // $respuesta = Respuesta::where('texto', $json_data->respuesta)->first();
        $respuesta = $pregunta->respuestas->where('texto', $json_data->respuesta)->first();
        
        //Actualizo temperaturas(de la sala y el equipo).
        Sala::actualizarTemp($id_sala, $idUser, $respuesta->grados);
        
        User::incrementarBienestar($idUser, $id_sala);

        AccionRealizada::registrar($idUser, $json_data->elemento_asociado, $id_sala, $respuesta->isOptima, $json_data->respuesta);

        /* */
        $response = Sala::refrescarJuego($request, false);
        /* */

        //Guardo los atributos de respuesta del fetch
        $response["justificacion"] = $pregunta->justificacion;

        if ($respuesta->isOptima == 1)
            $response["justificacion"] = __("PreguntasRespuestas.Correcta");
            
        $response["correcta"] = $respuesta->isOptima;

        return json_encode($response);
    }

    public static function validarPeticion($idUser, $idSala, $elementoAsociado, $respuesta)
    {
        if (!User::usuarioActivo($idSala, $idUser)) {
            $response = [
                        "valida" => false,
                        "justificacion" => __("PreguntasRespuestas.Bienestar"),
                        ];
            return $response;
        }

        if (!AccionRealizada::verificar_timestamp($idUser, $elementoAsociado, $idSala)) {
            $response = [
                        "valida" => false,
                        "justificacion" => __("PreguntasRespuestas.Timeout"),
                        ];
            return $response;
        }

        if (!Pregunta::verificar_etapa($elementoAsociado, $idSala)) {
            $response = [
                        'valida' => false,
                        'justificacion' => __("PreguntasRespuestas.Etapa"),
                        ];
            return $response;
        }

        if (!Sala::jugable($idSala)) {
            $response = [
                'valida' => false,
                'justificacion' => __("PreguntasRespuestas.Jugable"),
                ];
            return $response;
        }

        if ($respuesta == "") {
            $response = [
                'valida' => false,
                'justificacion' => __("PreguntasRespuestas.Seleccion"),
                ];
            return $response;
        }

        return ["valida" => true];
    }

    public static function crear($request){
        if ($request["idioma"] == "es"){
            $from = "es";
            $to = "en";
        } else{
            $from = "en";
            $to = "es";
        }

        $respuestas = Respuesta::obtenerRespuestas($request);

        $listToTranslate = [$request["enunciado"], $request["justificacion"]];

        forEach($respuestas as $respuesta){
            array_push($listToTranslate, $respuesta[1]);
        }
        
        $translatedValues = Self::translate($listToTranslate, $from, $to);

        $preguntaTraducida = Self::create([
            "elemento_asociado" => $request["elemento_asociado"],
            "enunciado" => $translatedValues[0]["translatedText"],
            "justificacion" =>$translatedValues[1]["translatedText"],
            "language" => $to,
            "etapa" => $request["etapa"],
        ]);

        $pregunta = Self::create([
            "elemento_asociado" => $request["elemento_asociado"],
            "enunciado" => $request["enunciado"],
            "justificacion" =>$request["justificacion"],
            "language" => $from,
            "etapa" => $request["etapa"],
        ]);

        Respuesta::crear($pregunta->id, $respuestas, $preguntaTraducida->id, $translatedValues);
    }

    public static function translate($text, $from, $to){
        try {
            $response = Http::withHeaders([
            "Content-Type" => "application/json; charset=utf-8"
        ])->post('https://translation.googleapis.com/language/translate/v2?key=AIzaSyDNN4TCrO3DbbR6Wy2TQOl_BZfwxW0aRR4', [
            "q" => $text,
            "target" => $to,
            "source" => $from,
        ]);

            $translated_text = $response["data"]["translations"];

            $result = [];

            $index = 0;
            forEach($translated_text as $translated){
                $result[$index]["translatedText"] = str_replace("&#39;", "'", $translated["translatedText"]);
                $index = $index+1;
            }

            return $result;
        } catch (Exception $e){
            return "";
        }
    }
}
