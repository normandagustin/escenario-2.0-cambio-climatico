<?php

namespace App\Models;

use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class Sala extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = "salas";

    protected $primaryKey = "id";

    public function stops()
    {
        return $this->belongsToMany(Stop::class)->withPivot('estado');
    }

    public function equipos()
    {
        return $this->hasMany(Equipo::class);
    }

    public function anfitrion()
    {
        return $this->hasOne('App\Models\User', "id", "id_anfitrion");
    }

    public static function crear(
        $request
    ) {
        try {
            $nueva_sala = self::create([
                'nombre'       => $request['sala_nombre'],
                'tiempoJuego'  => $request['sala_tiempoJuego'],
                'numJugadores'  => $request["sala_numJugadores"],
                'dificultad'   => $request["sala_dificultad"],
                'temperaturaGlobal' => 50,
                'etapaActual' => "maniana",
                'estado' => 'espera',
                'id_anfitrion' => auth()->user()->id,
                'timestampCreacionSala' => new DateTime(),
            ]);

            Equipo::crear_equipos($nueva_sala->id);

            Cookie::queue('cook_id_sala', $nueva_sala->id, 1440);

            return $nueva_sala;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function eliminar($request)
    {
        $json_data = json_decode($request->getContent());
        try {
            if (isset($json_data->id_sala)){
                $sala = Sala::where("id", $json_data->id_sala)->where("id_anfitrion", auth()->user()->id)->first();    
            }else{
                $sala = Sala::where("id", $request->cookie('cook_id_sala'))->where("id_anfitrion", auth()->user()->id)->first();
            }  

            Sala::where('id', $sala->id)->delete();
            Equipo::where('sala_id', $sala->id)->delete(); 
            
            if (!isset($sala)) {
                return ["eliminada" => false];
            }

            return ["eliminada" => true];
        } catch (Exception $e) {
            return ["eliminada" => false];
        }
    }

    public static function verificarContinuidadSala($id_sala)
    {
        try {
            $sala = Self::find($id_sala);
            if ($sala->id_anfitrion == auth()->user()->id) {
                return true;
            }
            
            return Equipo::pertenece($sala->equipos[0], auth()->user()->id) or Equipo::pertenece($sala->equipos[1], auth()->user()->id);
        } catch (Exception $e) {
            return false;
        }
    }


    public static function encontrar($request)
    {   
        $sala = self::find($request["id_sala"]);
        $sala["dificultad"] = __('CreadorSalas.'.$sala["dificultad"]);
        Cookie::queue('cook_id_sala', $request['id_sala'], 1440);

        return $sala;
    }

    public static function es_anfitrion($id_sala)
    {
        try {
            return self::find($id_sala)->id_anfitrion == auth()->user()->id;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getEstado($id_sala)
    {
        try {
            return self::find($id_sala)->estado;
        } catch (Exception $e) {
            return "";
        }
    }

    public static function jugable($idSala)
    {
        return self::getEstado($idSala) == "jugando";
    }

    public static function refrescarSala($request)
    {
        $id_sala = $request->cookie('cook_id_sala');
        $sala = self::find($id_sala);

        $continuidad = self::verificarContinuidadSala($id_sala);
        $equipos = [
            "equipos" => Equipo::obtener_equipos($id_sala),
            "is_anfi" => self::es_anfitrion($id_sala)
        ];

        $sala["dificultad"] = __('CreadorSalas.'.$sala["dificultad"]);

        $estado = self::getEstado($id_sala);

        return ["config" => $sala, "equipos" => $equipos, "continuidad" => $continuidad, "estado" => $estado];
    }

    public static function cantidadJugadores($sala)
    {
        return count($sala->equipos[0]->users) + count($sala->equipos[1]->users);
    }

    public static function garbageCollector()
    {
        foreach (self::all() as $sala) {
            if ($sala->estado == "espera") {
                $tiempoEsperaSala = self::enSegundos(date_diff(new DateTime($sala->timestampCreacionSala), new DateTime()));
                if ($tiempoEsperaSala >= 3600) {
                    $sala->delete();
                }
            }

            $tiempoSalaInactiva = self::enSegundos(date_diff(new DateTime($sala->timestampUltimoRefresh), new DateTime()));
            if ($tiempoSalaInactiva >= 3600) {
                $sala->delete();
            }
        }
    }

    public static function obtenerSalas()
    {
        self::garbageCollector();
        $salas = self::where("estado", "espera")->get();
        foreach ($salas as $sala) {
            $sala["player_count"] = self::cantidadJugadores($sala);
            $sala["is_anfi"] = $sala->id_anfitrion == auth()->user()->id;
            $sala["dificultad"] = __('CreadorSalas.'.$sala["dificultad"]);
        }
        return $salas;
    }

    public static function actualizar($request)
    {
        //$sala = self::find($request["id_sala"]);
        $sala = self::find($request->cookie('cook_id_sala'));

        $sala->tiempoJuego = $request["tiempo"];
        $sala->numJugadores = $request["jugadores"];
        $sala->dificultad = $request["dificultad"];

        $sala->save();

        return true;
    }

    public static function salirSala($request)
    {
        $sala = self::find($request->cookie('cook_id_sala'));
        if (!self::es_anfitrion($sala->id)) {
            $equipo = Equipo::obtenerEquipo($sala, auth()->user()->id);
            $equipo->users()->detach(auth()->user()->id);
        }
    }
    ///

    
    /* */
    /* Codigo que a futuro, deberíamos separarlo a una "Partida" */
    /* */

    public static function iniciar($request)
    {
        $sala = Sala::find($request->cookie('cook_id_sala'));
        if (self::es_anfitrion($sala->id)) {
            $sala->estado = "jugando";

            $timestampInicioJuego = new DateTime();

            $sala->timestampInicioJuego = $timestampInicioJuego;

            foreach ($sala->equipos as $equipo) {
                foreach ($equipo->users as $user) {
                    $user->equipos()->updateExistingPivot($equipo->id_equipo, [
                        'tiempoTranscurridoDecrementoBienestar' => $timestampInicioJuego
                    ]);
                }
            }

            $sala->save();
        }
        return $sala;
    }

    public static function refreshNecesario($sala)
    {
        if (is_null(($sala->timestampUltimoRefresh))) {
            return true;
        }

        $ahora = new DateTime();
        $timestampUltimoRefresh = new DateTime($sala->timestampUltimoRefresh);
        $seconds = self::enSegundos(date_diff($timestampUltimoRefresh, $ahora));

        return $seconds >= 1;
    }

    public static function refrescarJuego($request, $encode = true)
    {
        $response = [];

        $sala = self::find($request->cookie('cook_id_sala'));

        if (self::refreshNecesario($sala)) {
            self::verificarCambioEtapa($sala);

            self::actualizarBienestares($sala);

            self::revisarEstadoJugadores($sala);

            $sala->timestampUltimoRefresh = new DateTime();
            $sala->save();
        }

        $usuario_autenticado = auth()->user()->id;

        if ($usuario_autenticado == $sala->id_anfitrion) {
            $response = [
                "tEquipo1" => $sala->equipos[0]->temperaturaLocal,
                "tEquipo2" => $sala->equipos[1]->temperaturaLocal,
                "tempGlobal" => $sala->temperaturaGlobal,
                "etapa" => __("Salas.".$sala->etapaActual),
                "estado" => $sala->estado,
            ];
            if ($encode) {
                return json_encode($response);
            } else {
                return $response;
            }
        }
        
        self::verStopContestado($sala, $response);
        // revisar votacion
        self::revisarStop($response, $sala);
        
        $equipo = Equipo::obtenerEquipo($sala, $usuario_autenticado);

        $usuario_is_activo = User::usuarioActivo($sala->id, $usuario_autenticado);

        // Si el usuario esta activo.
        self::addFieldToResponse($response, [
            "tempLocal" => $equipo->temperaturaLocal,
            "tempGlobal" => $sala->temperaturaGlobal,
            "estado" => $sala->estado,
            "elementos_disponibles" => Pregunta::obtener_elementos_disponibles($sala->id),
            "etapa" => __("Salas.".$sala->etapaActual),
            "bienestar" => User::obtenerBienestar($usuario_autenticado, $sala->id),
            "usuario_activo" => $usuario_is_activo,
            "tiempoJugando" => self::obtenerSegundosJuego($sala),
            "tiempoJuegoSala" => $sala->tiempoJuego,
        ]);

        if ($sala->estado == "stop") {
            $response = self::agregarStop($response, $sala->id);
        }

        if (!$usuario_is_activo) { // Si el usuario esta inactivo
            $response["elementos_disponibles"] = array();
        }

        if ($encode) {
            return json_encode($response);
        } else {
            return $response;
        }
    }

    /* */
    /* Codigo relacionado al cambio de etapa */
    /* */
    
    public static function addFieldToResponse(&$response, $array)
    {
        foreach ($array as $key => $campo)
            $response[$key] = $campo;
    }

    public static function obtenerSegundosJuego($sala)
    {
        $ahora = new DateTime();
        $timestampInicioJuego = new DateTime($sala->timestampInicioJuego);
        return self::enSegundos(date_diff($timestampInicioJuego, $ahora));
    }

    public static function verificarCambioEtapa($sala)
    {
        $seconds = self::obtenerSegundosJuego($sala);

        $tiempoEtapa = ($sala->tiempoJuego/3)*60;

        if ($seconds < $tiempoEtapa) {
            return $tiempoEtapa - $seconds;
        }
        
        if ($seconds > $tiempoEtapa and $seconds <= ($tiempoEtapa * 2)) {
            return ($tiempoEtapa * 2) - $seconds;
            self::siEstaEnCambiarA($sala, "maniana", "mediodia");
        }
        if ($seconds > ($tiempoEtapa * 2) and $seconds <= ($tiempoEtapa * 3)) {
            return ($tiempoEtapa * 3) - $seconds;
            self::siEstaEnCambiarA($sala, "mediodia", "tarde");
        }
        if ($seconds > ($tiempoEtapa * 3)) {
            self::siEstaEnCambiarA($sala, "tarde", "finalizado");
        }
    }

    public static function enSegundos($intervalo)
    {
        $daysInSecs = $intervalo->format('%r%a') * 24 * 60 * 60;
        $hoursInSecs = $intervalo->h * 60 * 60;
        $minsInSecs = $intervalo->i * 60;

        return $daysInSecs + $hoursInSecs + $minsInSecs + $intervalo->s;
    }

    public static function siEstaEnCambiarA($sala, $etapaActual, $etapaNecesaria)
    {
        if ($etapaNecesaria == "finalizado") {
            if ($sala->estado != "finalizado") {
                $sala->estado = "finalizado";
                $sala->save();
            }
        } elseif ($sala->etapaActual == $etapaActual) {
            $sala->etapaActual = $etapaNecesaria;
            $sala->save();
        }

        return true;
        //Esto lo quiero usar para ver si en algun momento se produjo algun error cambiando de etapas.
        //TODO
        //Si esta en la 1, y tiene que estar en la 2, cambialo
        //Si esta en la 3 y tiene que estar en la 2, no lo cambies.
    }

    /* */
    /* Fin codigo relacionado al cambio de etapa */
    /* */

    public static function actualizarBienestares($sala)
    {
        if ($sala->estado != "stop") {
            foreach ($sala->equipos as $equipo) {
                foreach ($equipo->users as $user) {
                    User::decrementarBienestar($user, $sala);
                }
            }
        }
    }
    
    /* */
    /* Fin codigo que a futuro, deberíamos separarlo a una "Partida" */
    /* */
    
    /* */
    /* Esta funcion recorre todos los usuarios, y les cambia el tiempoTranscurridoDecrementoBienestar a "ahora" */
    /* Esta funcion solo se ejecuta cuando el estado pasa de "jugando" a "stop" */
    /* Lo separé de la función anterior porque solo pasa por aqui una vez por stop */
    /* */
    public static function actualizarBienestaresFinStop($sala)
    {
        foreach ($sala->equipos as $equipo) {
            foreach ($equipo->users as $user) {
                // cambia el tiempoTranscurridoDecrementoBienestar
                User::actualizarLastDec($user->id, $sala);
            }
        }
    }
    /* */
    /* Fin codigo que a futuro, deberíamos separarlo a una "Partida" */
    /* */

    public static function verStopContestado($sala)
    {
        if ($sala->estado == "stop") {
            $stop = $sala->stops->where('pivot.estado', 'abierto')->first();
            $timeCreado = new DateTime($stop->created_at);
            $timeAhora =  new DateTime();
            $segundosStop = $stop->minutos * 60;
            $segundosDiff = self::enSegundos(date_diff($timeCreado, $timeAhora));
            if ($segundosDiff > $segundosStop) {
                // actualizar last decrement cuando termina el stop
                self::actualizarBienestaresFinStop($sala);
                $response = null;
                $sala->estado = "jugando";
                $sala->save();
                self::realizarVotacion($sala);
            }
        }
    }
    

    public static function stopSala($id_sala)
    {
        $sala = self::find($id_sala);
        $sala->estado = 'stop';
        $sala->save();
    }

    public static function finalizarSala($id_sala)
    {
        $sala = self::find($id_sala);
        $sala->estado = "finalizado";
        $sala->save();
    }

    public static function iniciarSala($id_sala)
    {
        $sala = self::find($id_sala);
        $sala->estado = "jugando";
        $sala->save();
    }

    public static function obtenerTempGlobal($id_sala)
    {
        return self::find($id_sala)->temperaturaGlobal;
    }

    public static function actualizarTemperaturaGlobal($id_sala, $grados)
    {
        $sala = self::find($id_sala);
        $sala->temperaturaGlobal = $sala->temperaturaGlobal + $grados;
        $sala->save();
    }

    public static function actualizarEstadoJuego($id_sala)
    {
        $tempGlobal = self::obtenerTempGlobal($id_sala);

        if ($tempGlobal >= 100.00) {
            self::finalizarSala($id_sala);
        }
    }

    public static function actualizarTemp($id_sala, $id_usuario, $grados)
    {
        $sala = self::find($id_sala);
        $equipo = Equipo::obtenerEquipo($sala, $id_usuario);

        if ($equipo !== null) {
            //Actualizando temperatura Local
            Equipo::actualizarTemperatura($id_sala, $id_usuario, $grados);
            
            //Actualizando temperatura Global
            self::actualizarTemperaturaGlobal($id_sala, $grados);

            //Verificar y actualizar estado del juego.
            self::actualizarEstadoJuego($id_sala);
        }

        //Se registra en el jugador cuantos grados generó
        Equipo::registrarGradosJugador($id_usuario, $equipo->id_equipo, $grados);
    }

    public static function getStop($id_sala)
    {
        $sala = self::find($id_sala);
        return $sala->stops->where('pivot.estado', 'abierto')->first();
    }

    public static function finJuego($id_sala, $idUser)
    {

        // TODO Validar que el usuario pertenezca a esa sala

        // TODO Mostrar más estadisticas del jugador.

        $array_data = [];

        $sala = Sala::find($id_sala);

        if ($sala->estado == "finalizado") {
            if ($sala->temperaturaGlobal >= 100) {
                $array_data["salvado"] = False;
                $array_data["titulo"] = __('Final.Perdido');
            } else {
                $array_data["titulo"] = __('Final.Salvado');
                $array_data["salvado"] = True;
            }
        } else {
            $array_data["titulo"] = __('Final.EnCurso');
        }

        $user = User::find($idUser);

        $equipo = Equipo::obtenerEquipo($sala, $idUser);

        $equipoContrario = Equipo::obtenerEquipoContrario($sala, $idUser);

        //$array_data["tiempoTotalJuego"] = 0; //TODO

        $array_data["temperaturaGlobalSala"] = $sala->temperaturaGlobal;

        $array_data["temperaturaLocalEquipo"] = $equipo->temperaturaLocal;

        $array_data["temperaturaLocalEquipoContrario"] = $equipoContrario->temperaturaLocal;

        $array_data["cantidadPreguntasRespondidas"] = AccionRealizada::cantidadRespuestas($idUser, $id_sala);

        $array_data["cantidadPreguntasOptimas"] = AccionRealizada::cantidadRespuestasOptimas($idUser, $id_sala);

        $array_data["cantidadBienestar"] = User::obtenerBienestar($idUser, $id_sala);

        $array_data["cantidadGradosGenerados"] = User::obtenerGradosGenerados($idUser, $id_sala);

        $array_data["resultado"] = Equipo::obtenerResultado($equipo, $equipoContrario);

        $array_data["accionesIncorrectas"] = AccionRealizada::accionesIncorrectas($id_sala);
        
        $array_data["respuestasIncorrectas"] = AccionRealizada::respuestasIncorrectas($idUser, $id_sala);

        return $array_data;
    }


    public static function finJuegoAnfitrion($id_sala, $idUser)
    {

        // TODO Validar que el usuario pertenezca a esa sala

        // TODO Mostrar más estadisticas del jugador.

        $array_data = [];

        $sala = Sala::find($id_sala);

        if ($sala->estado == "finalizado") {
            if ($sala->temperaturaGlobal >= 100) {
                $array_data["titulo"] = "El mundo no pudo salvarse.";
            } else {
                $array_data["titulo"] = "¡Felicidades! Lograron salvar al mundo.";
            }
        } else {
            $array_data["titulo"] = "La partida sigue en curso.";
        }

        $array_data["temperaturaGlobalSala"] = $sala->temperaturaGlobal;

        $array_data["accionesIncorrectas"] = AccionRealizada::accionesIncorrectas($id_sala);

        return $array_data;
    }
    //Revisa y actualiza el estado del juego si hay un o mas equipos con jugadores inactivos.
    public static function revisarEstadoJugadores($sala)
    {
        $activos = false;
        $equipos = $sala->equipos;
        foreach ($equipos as $equipo) {
            if (!Equipo::equipoActivo($equipo)) {
                $activos = true;
            }
        }

        if ($activos) {
            self::finalizarSala($sala->id);
        }
    }

    public static function agregarStop($response, $sala_id)
    {
        $stop = self::getStop($sala_id);
        $opcionesStop = $stop->respuestas;
        $opciones = [];
        foreach ($opcionesStop as $opcion) {
            array_push($opciones, $opcion->texto);
        }

        $response["enunciadoStop"] = $stop->enunciado;
        $response["opciones"] = $opciones;
        
        return $response;
    }

    // $cambio = true --> debe hacer el cambio de temperaturas
    public static function revisarStop(&$response, $sala)
    {
        if($sala->estado == "jugando"){
            // Quitare el atributo, y hago el join eloquent de la siguiente forma
            $stop = self::getStop($sala->id);
            if($stop != null){
                $votosStop = self::getVotos($stop);
    
                $equipo = Equipo::obtenerEquipo($sala, auth()->user()->id);
        
                $votoFinal = self::getVotoFinal($sala, $equipo, $votosStop); 
    
                self::revisarVotacionFinal($response, $votoFinal, $stop);
            }
        }
    }

    public static function getVotos($stop)
    {
        $respuestasStop = $stop->respuestas;
        $votosStop = [];
        foreach ($respuestasStop as $respuesta) {
            array_push($votosStop, $respuesta->votos);
        } // en $votosStop tendria un array en el que cada elemento es un array con los votos_user de cada respuesta 
        return $votosStop;
    }

    public static function getVotoFinal($sala, $equipo, $votosStop)
    {
        $votosEquipo = []; // array asociativo, id => numVotos
        foreach ($votosStop as $votoRespuesta) {
            foreach ($votoRespuesta as $voto) {
                if (Equipo::obtenerEquipo($sala, $voto->pivot->user_id)->id_equipo == $equipo->id_equipo) {
                    $respuestaStop = StopRespuesta::find($voto->pivot->stop_respuesta_id);
                    $key = $respuestaStop->id;
                    if (!array_key_exists($key, $votosEquipo)) {
                        $votosEquipo[$key] = 0;
                    }
                    $votosEquipo[$key] ++;
                }
            }
        }
        array_keys($votosEquipo, max($votosEquipo));
        return $votosEquipo;
    }

    public static function revisarVotacionFinal(&$response, $votoFinal, $stop)
    {
        if(sizeof($votoFinal) == 1){
            // Solo una respuesta fue votada
            // ver si es la optima/correcta
            $idRespuestaFinal = array_keys($votoFinal);
            $respuestaFinal = StopRespuesta::find($idRespuestaFinal[0]);
            $correcto = false;
            if($respuestaFinal->isOptima)
                $correcto = true;
        }else{
            // Dos o mas respuestas fueron mas votadas.
            $correcto = false;
        }
        self::addFieldToResponse($response, ["correcto" => $correcto]);
        self::addFieldToResponse($response, ["respuesta_Optima" => Stop::getRespuestaOptima($stop)]);
        self::addFieldToResponse($response, ["justificacion" => $stop->justificacion]);
    }

    // Realiza los cambios en las temperaturas de los equipos lueg de la votacion Stop
    public static function realizarVotacion($sala)
    {
        $stop = self::getStop($sala->id);
        $votosStop = self::getVotos($stop);
        foreach ($sala->equipos as $equipo) {
            $votoFinal = self::getVotoFinal($sala, $equipo, $votosStop); // puede ser más, en cuyo caso subir temperatura
            
            if(sizeof($votoFinal) == 1){
                $idRespuestaFinal = array_keys($votoFinal);
                $respuestaFinal = StopRespuesta::find($idRespuestaFinal[0]);
                if($respuestaFinal->isOptima){
                    // bajar temperatura al equipo --> Stop->temperatura
                    $grados = $equipo->temperaturaLocal - $stop->grados;
                    if($grados < 0)
                        $grados = 0;
                    $equipo->temperaturaLocal = $grados;
                    $equipo->save();
                }else{
                    // subir temperatura al equipo --> Stop->temperatura
                    $grados = $equipo->temperaturaLocal + $stop->grados;
                    if($grados > 100)
                        $grados = 100;
                    $equipo->temperaturaLocal = $grados;
                    $equipo->save();
                }
            }else{
                // subir temperatura al equipo --> Stop->temperatura
                $grados = $equipo->temperaturaLocal + $stop->grados;
                if($grados > 100)
                    $grados = 100;
                $equipo->temperaturaLocal = $grados;
                $equipo->save();
            }
        }


    }

    /*static public function setearCookiesSalaCreada($sala){
        $cookie_id_sala = "cook_id_sala";
        $cookie_id_sala_value   = $sala->id;
        setcookie($cookie_id_sala, $cookie_id_sala_value, time() + (86400 * 30), "/"); // 86400 = 1 day
    }*/

}
