<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = "equipos";

    protected $primaryKey = 'id_equipo';
    
    public function sala()
    { //asume por el nombre que la foreign key es sala_id
        return $this->belongsTo(Sala::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, "users_equipos", "equipo_id", "user_id")
            ->withPivot('bienestar', 'gradosGenerados', 'estado', 'tiempoTranscurridoDecrementoBienestar');
    }

    public static function crear_equipos($sala_id)
    {
        self::create(["sala_id" => $sala_id, "temperaturaLocal" => 0]);
        self::create(["sala_id" => $sala_id, "temperaturaLocal" => 0]);
    }

    public static function pertenece($equipo, $id_usuario)
    {
        // return count(User_Equipo::where("user_id", $id_usuario)->where("equipo_id", $equipo->id_equipo)->get()) != 0;
        $eq = self::find($equipo->id_equipo);
        $count = $eq->users->where('pivot.user_id', $id_usuario)->count();
        return $count != 0;
    }

    public static function obtenerEquipo($sala, $usuario_autenticado)
    {
        if (self::pertenece($sala->equipos[0], $usuario_autenticado)) {
            return $sala->equipos[0];
        }
        if (self::pertenece($sala->equipos[1], $usuario_autenticado)) {
            return $sala->equipos[1];
        }
        return null;
    }

    public static function obtenerEquipoContrario($sala, $usuario_autenticado)
    {
        if (self::pertenece($sala->equipos[0], $usuario_autenticado)) {
            return $sala->equipos[1];
        }
        if (self::pertenece($sala->equipos[1], $usuario_autenticado)) {
            return $sala->equipos[0];
        }
        return null;
    }

    public static function efectuar_ubicar_en_equipo($equipos, $sala)
    {
        $insertado = false;
        $equipoElegido = 0;

        // $cantJugEq1 = User_Equipo::where('equipo_id', $equipos[0]->id_equipo)->count();
        $equipo1 = self::find($equipos[0]->id_equipo);
        $cantJugEq1 = $equipo1->users->count();

        if ($sala->numJugadores > $cantJugEq1) {
            //Insertamos en el 1er Equipo al usuario
            $equipoElegido = $equipos[0]->id_equipo;
            $insertado = true;
        } else {
            // $cantJugEq2 = User_Equipo::where('equipo_id', $equipos[1]->id_equipo)->count();
            $equipo2 = self::find($equipos[1]->id_equipo);
            $cantJugEq2 = $equipo2->users->count();
            if ($sala->numJugadores > $cantJugEq2) {
                //Insertamos en el 2do Equipo al usuario
                $equipoElegido = $equipos[1]->id_equipo;
                $insertado = true;
            }
        }

        if ($insertado) { // Si ya se tomo la decision de insertar.
            // User_Equipo::insert([
            //     'equipo_id' => $equipoElegido,
            //     'user_id'   => auth()->user()->id,
            //     'bienestar' => 100,
            //     'gradosGenerados' => 0,
            //     'estado' => 'activo',
            // ]);
            $equipo_insert = self::find($equipoElegido);
            $equipo_insert->users()->attach(auth()->user()->id, ['bienestar' => 100, 'gradosGenerados' => 0]);
        }
        return $insertado;
    }

    public static function noTieneEquipo($equipos, $id_jugador)
    {
        foreach ($equipos as $equipo) {
            // $equipos = User_Equipo::where("user_id", $id_jugador)
            //                       ->where("equipo_id", $id_equipo)
            //                       ->get();
            $usuarios = $equipo->users->where('pivot.user_id', $id_jugador);
            if (count($usuarios) != 0) {
                return false;
            }
        }
        return true;
    }

    public static function ubicar_en_equipo($request)
    {
        $sala = Sala::find($request["id_sala"]);
        if ($sala->id_anfitrion != auth()->user()->id) {
            if (self::noTieneEquipo($sala->equipos, auth()->user()->id)) {
                return self::efectuar_ubicar_en_equipo($sala->equipos, $sala);
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static function getTemperaturaLocal($id_equipo)
    {
        $equipo = self::find($id_equipo);
        return $equipo->temperaturaLocal;
    }

    public static function obtener_equipos($id_sala)
    {
        try {
            $sala = Sala::find($id_sala);
            return [
                "jugadores_totales" => $sala->numJugadores,
                "equipo_1" => $sala->equipos[0],
                "equipo_2" => $sala->equipos[1],
                "jugadores_equipo_1" => $sala->equipos[0]->users,
                "jugadores_equipo_2" => $sala->equipos[1]->users,
            ];
        } catch (Exception $e) {
            return [
                "jugadores_totales" => "",
                "equipo_1" => "",
                "equipo_2" => "",
                "jugadores_equipo_1" => "",
                "jugadores_equipo_2" => "",
            ];
        }
    }

    public static function actualizarTemperatura($id_sala, $usuario_autenticado, $grados)
    {
        $sala = Sala::find($id_sala);
        $equipo = self::obtenerEquipo($sala, $usuario_autenticado);
        $equipo->temperaturaLocal = $equipo->temperaturaLocal + $grados;
        $equipo->save();
    }

    public static function registrarGradosJugador($id_usuario, $id_equipo, $grados)
    {
        $usuario = User::find($id_usuario);
        $equipo = $usuario->equipos->where('id_equipo', $id_equipo)->first();
        $gradosGenerados = $equipo->pivot->gradosGenerados + $grados;
        $usuario->equipos()->updateExistingPivot($equipo->id_equipo, [
            'gradosGenerados' => $gradosGenerados
        ]);
    }

    public static function moverJugador($request)
    {
        // request --> id_sala e id_user
        $id_sala = $request->cookie('cook_id_sala');
        $request_data = json_decode($request->getContent());
        $user = User::find($request_data->id_user);
        $sala = Sala::find($id_sala);
        try {
            $equipo_quitar = $user->equipos->where('sala_id', $sala->id)->first();
            $user->equipos()->detach($equipo_quitar);

            $equipo_poner = $sala->equipos->where('id_equipo', '!=', $equipo_quitar->id_equipo)->first();
            $user->equipos()->attach($equipo_poner->id_equipo, ['bienestar' => 100, 'gradosGenerados' => 0]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function kickearJugador($request)
    {
        $request_data = json_decode($request->getContent());
        $user = User::find($request_data->id_user);
        try {
            $equipo = $user->equipos->where('sala_id', $request->cookie('cook_id_sala'))->first();
            $user->equipos()->detach($equipo->id_equipo);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public static function obtenerResultado($equipoUno, $equipoDos)
    {
        if ($equipoUno->temperaturaLocal == $equipoDos->temperaturaLocal) {
            return "empataron";
        }

        if ($equipoUno->temperaturaLocal < $equipoDos->temperaturaLocal) {
            return "ganaron";
        }

        if ($equipoUno->temperaturaLocal > $equipoDos->temperaturaLocal) {
            return "perdieron";
        }
    }

    public static function equipoActivo($equipo){
        $activo = false;
        foreach ($equipo->users as $user) {
            if($user->pivot->estado == "activo"){
                $activo = true;
            }
        }
        return $activo;
    }
}
