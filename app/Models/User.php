<?php

namespace App\Models;

use DateTime;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $primaryKey = "id";

    protected $table = "users";

    public function accionesRealizadas()
    { 
        return $this->hasMany(AccionRealizada::class);
    }

    //encriptación de la contraseña
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function equipos()
    {
        return $this->belongsToMany(Equipo::class, "users_equipos", "user_id", "equipo_id")
            ->withPivot('bienestar', 'gradosGenerados','estado');
    }

    public function salas()
    {
        return $this->hasMany(Sala::class);
    }

    public function votos()
    {
        return $this->belongsToMany(StopRespuesta::class, "votos_users", "user_id", "stop_respuesta_id");
    }

    //Devuelve el nombre con la primera letra en mayuscula
    public function getNameAttribute($name)
    {
        return ucwords($name);
    }

    public function roles()
    {
        return $this->belongsToMany(Rol::class)->withTimestamps();
    }

    public function asignarRol($rol)
    {
        $this->roles()->sync($rol, false); //Con false le indicamos que no elimine registros
        //Con sync le indicamos que si yua existía el registro lo reemplace 
        //(es para evitar que de error porque ya existía un registro con esda PK) 
    }

    public function permisos()
    { //Recupera los permisos que tiene un usuario en particular
        return $this->roles->map->permisos->flatten()->pluck('nombre')->unique();
    }

    // Recupera el equipo en el que el usuario está, según la sala
    public static function getEquipo($user_id, $sala_id)
    {
        $user = self::find($user_id);
        return $user->equipos()->where('sala_id', $sala_id)->get();
    }

    public static function obtenerBienestar($idUser, $idSala){
        $user = self::find($idUser);

        $equipo = $user->equipos->where('sala_id', $idSala)->first();

        return $equipo->pivot->bienestar;
    }

    public static function obtenerGradosGenerados($idUser, $idSala){
        $user = self::find($idUser);

        $equipo = $user->equipos->where('sala_id', $idSala)->first();

        return $equipo->pivot->gradosGenerados;
    }

    public static function incrementarBienestar($idUser, $idSala){
        $user = self::find($idUser);

        $equipo = $user->equipos->where('sala_id', $idSala)->first();

        $equipo->pivot->bienestar = $equipo->pivot->bienestar + 30;

        if ($equipo->pivot->bienestar >= 100)
            $equipo->pivot->bienestar = 100;

        $equipo->pivot->save();

        return $equipo->pivot->bienestar;
    }

    public static function decrementarBienestar($user, $sala)
    {
        $ahora = new DateTime();
        $tiempoDecrementoBienestar = new DateTime($user->pivot->tiempoTranscurridoDecrementoBienestar);
        $tiempoHastaAhora = self::enSegundos(date_diff($tiempoDecrementoBienestar, $ahora));
        $bienestarUsuario = $user->pivot->bienestar;
        $user->pivot->bienestar = $bienestarUsuario - ($tiempoHastaAhora * 0.3);
        if ($user->pivot->bienestar <= 0){
            $user->pivot->bienestar = 0;
            $user->pivot->estado = "inactivo";
        }
        // $user->pivot->tiempoTranscurridoDecrementoBienestar = $ahora;
        self::actualizarLastDec($user->id, $sala);
        $user->pivot->save();

        return $bienestarUsuario = $user->pivot->bienestar;
    }

    public static function enSegundos($intervalo)
    {
        $daysInSecs = $intervalo->format('%r%a') * 24 * 60 * 60;
        $hoursInSecs = $intervalo->h * 60 * 60;
        $minsInSecs = $intervalo->i * 60;

        return $daysInSecs + $hoursInSecs + $minsInSecs + $intervalo->s;
    }

    public static function usuarioActivo($id_sala,$id_usuario){
        $user = self::find($id_usuario);
        $equipo = $user->equipos->where('sala_id', $id_sala)->first();
        return $equipo->pivot->estado == 'activo';
    }

    public static function actualizarLastDec($user_id, $sala)
    {
        $equipo = Equipo::obtenerEquipo($sala, $user_id);
        $ahora = new DateTime();
        $equipo->users()->updateExistingPivot( $user_id, [
            'tiempoTranscurridoDecrementoBienestar' => $ahora,
        ]);
        $equipo->save();

    }
}
