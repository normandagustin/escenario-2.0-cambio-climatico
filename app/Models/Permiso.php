<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    //use HasFactory;
    //un permiso puede pertenecer a varios roles
    public function roles()
    {
        return $this->belongsToMany(Permiso::class)->withTimestamps();
    }
}
