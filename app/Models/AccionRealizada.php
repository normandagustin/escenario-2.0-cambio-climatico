<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\App;

class AccionRealizada extends Model
{
    use HasFactory;
    protected $table = "accion_realizadas";

    protected $guarded = [];

    //relacion uno a muchos(inversa).
    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public static function registrar($idUser, $elemento_asociado, $sala, $is_optima, $respuesta)
    {
        Self::create([
            "elemento_asociado" => $elemento_asociado,
            "id_sala" => $sala,
            "is_optima" => $is_optima,
            "id_user" => $idUser,
            "respuesta" => $respuesta,
            "done_at" => new DateTime(),
        ]);
    }

    public static function verificar_timestamp($usuario_autenticado, $elemento_asociado, $sala)
    {
        $return_value = false;
        $accion_realizada = Self::where("elemento_asociado", $elemento_asociado)
            ->where("id_user", $usuario_autenticado)
            ->where("id_sala", $sala)
            ->orderBy("done_at", "desc")
            ->first();

        if (is_null($accion_realizada)) {
            $return_value = true;
        } else {
            $date = new \DateTime();
            $date->setTimestamp(strtotime($accion_realizada->done_at));
            $diff = $date->diff(new \DateTime('now'));

            $daysInSecs = $diff->format('%r%a') * 24 * 60 * 60;
            $hoursInSecs = $diff->h * 60 * 60;
            $minsInSecs = $diff->i * 60;
            $seconds = $daysInSecs + $hoursInSecs + $minsInSecs + $diff->s;
            return $seconds > 120;
        }

        return $return_value;
    }

    public static function cantidadRespuestas($usuario_autenticado, $idSala){
        return Self::where("id_user", $usuario_autenticado)
                   ->where("id_sala", $idSala)
                   ->count();
    }

    public static function cantidadRespuestasOptimas($usuario_autenticado, $idSala){
        return Self::where("id_user", $usuario_autenticado)
                   ->where("id_sala", $idSala)
                   ->where("is_optima", 1)
                   ->count();
    }

    public static function accionesIncorrectas($id_sala){
        $elementos = Self::select('preguntas.enunciado', 'respuesta')
            ->join('preguntas', 'preguntas.elemento_asociado', '=', 'accion_realizadas.elemento_asociado')
            ->where("id_sala", $id_sala)//TODO hardcodeado para probar
            ->where("is_optima", 0)
            ->get();
        $respuestasContadas = $elementos->countBy(function ($item) {
                return __("Final.Pregunto"). " " . $item['enunciado'] . " ".__("Final.Respondieron").": " . $item['respuesta'] ;
            });
        $respuestasContadas = $respuestasContadas->sortDesc();
        return $respuestasContadas;//Se devuelve en forma de array para trabajarlo con php
    }

    public static function respuestasIncorrectas($id_user, $id_sala){
        $respuestas = Self::select('preguntas.enunciado', 'respuesta')
            ->join('preguntas', 'preguntas.elemento_asociado', '=', 'accion_realizadas.elemento_asociado')
            ->where("id_user", $id_user)
            ->where("id_sala", $id_sala)
            ->where("is_optima", 0)
            ->where("language", App::getLocale())
            ->get();
        $respuestasContadas = $respuestas->countBy(function ($item) {
            return __("Final.Pregunto"). " " . $item['enunciado'] . " ".__("Final.Respondiste").": " .  $item['respuesta'] ;
        });
        $respuestasContadas = $respuestasContadas->sortDesc();
        return $respuestasContadas;  
    }

}
