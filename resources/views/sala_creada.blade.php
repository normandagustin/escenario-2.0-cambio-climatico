@extends('layout')

@section('nombre_y_estilos')
    <title>{{ __('SalaCreada.Titulo') }}_{{ $sala->nombre }}</title>
    <link rel="stylesheet" href="/css/sala_creada.css">
    <script src="/js/sala_creada.js"> </script>
    <script src="/js/paw.js"> </script>
@endsection
    

@section('cuerpo_principal')
    <x-rotar-pantalla /> 
    <section class="dificultad_titulo">
        <h1>{{ __('SalaCreada.Bienvenidos') }} {{ $sala->nombre }} </h1>
        <h2>{{ __('SalaCreada.Anfitrion') }} {{ $sala->anfitrion->name }} </h2>
        @if ($sala->id_anfitrion == auth()->user()->id)
            <span class="config_icon"></span>
        @endif
    </section>

    <form id="crear_sala_form" action="/iniciarJuego" method="POST">

        @csrf
        <section class="config_text">
            <p class="tiempo_juego"> {{ __('SalaCreada.TiempoJuego') }} {{ $sala->tiempoJuego }}</p>
            <p class="cant_jugadores"> {{ __('SalaCreada.CantidadJugadores') }} {{ $sala->numJugadores }} </p>
            <p class="dificultad"> {{ __('SalaCreada.Dificultad') }} {{ $sala->dificultad }} </p>
        </section>
        <section class="listas">
            <section class="equipos" id="section_equipo1">
                <h3 id="equipo_1_tag">{{ __('SalaCreada.Equipo1') }} (0/{{ $sala->numJugadores }})</h3>
                <ul id="equipo_1_list"></ul>
            </section>
            <section class="equipos" id="section_equipo2">
                <h3 id="equipo_2_tag">{{ __('SalaCreada.Equipo2') }} (0/{{ $sala->numJugadores }})</h3>
                <ul id="equipo_2_list"></ul>
            </section>
        </section>
    </form>

    <fieldset class="section_botones">
        <button class="boton" id="salir_sala">{{ __('SalaCreada.Salir') }}</button>
        @if ($sala->id_anfitrion == auth()->user()->id)
            <button class="boton btn_eliminar_sala" id="eliminar_sala">{{ __('SalaCreada.Eliminar') }}</button>
            <input class="boton" type="submit" name="submit_crear" id="submit_crear" form="crear_sala_form"
                value={{ __('SalaCreada.Empezar') }}>
        @endif
    </fieldset>
    @if ($sala->id_anfitrion !== auth()->user()->id)
        <x-esperando-jugadores/>
    @endif
    <section id="pantalla-inactiva" class="ocultar">
        {{-- Este section es la pantalla en negro que aparece al abrir un pop-up --}}
    </section>

@endsection

@section('componentes')
    <x-configuracion-sala />
@endsection
