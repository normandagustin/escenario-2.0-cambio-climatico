@extends('layout')

@section('nombre_y_estilos')
    <title>{{ __('Final.TituloPagina') }}_{{ $sala->nombre }}</title>
        <link rel="stylesheet" href="/css/final.css" type="text/css">
@endsection

@section('cuerpo_principal')
    <h1>{{ __('Final.Titulo') }}</h1>
    @if ($data["salvado"])
        <h2>{{$data["titulo"]}}</h2>
            <div class="borde-video">
                <video controls >
                    <source src="../videos/planeta-salvado.mp4" type="video/mp4">
                    {{ __('Inicio.ErrorVideo') }}
                </video>
            </div>
    @else 
        <h2>{{$data["titulo"]}}</h2>
        <div class="borde-video">
            <video controls  autopictureinpicture>
                <source src="../videos/planeta-destruido.mp4" type="video/mp4">
                {{ __('Inicio.ErrorVideo') }}
            </video>
        </div>
    @endif
    @if ($data["resultado"] == "perdieron")
        <h3><strong>{{ __('Final.Perdieron') }}</strong></h3>
        <p>{{ __('Final.PerdieronTexto1') }}</p>
        <p>{{ __('Final.PerdieronTexto2') }}<p>
    @elseif ($data["resultado"] == "ganaron")
        <h3><strong>{{ __('Final.Perdieron') }}</strong></h3>
        <p>{{ __('Final.PerdieronTexto3') }}</p>
    @else
        <h3><strong>{{ __('Final.Ganaron') }}</strong></h3>
        <p>{{ __('Final.GanaronTexto1') }}</p>
        {{ __('Final.GanaronTexto2') }}
    @endif
    <ul>
        <li>{{ __('Final.TemperaturaGlobal') }}<strong> {{$data["temperaturaGlobalSala"]}} {{ __('Final.Grados') }} </strong></li>
        <li>{{ __('Final.TemperaturaEquipo') }} <strong>{{$data["temperaturaLocalEquipo"]}} {{ __('Final.Grados') }}</strong></li>
        <li>{{ __('Final.TemperaturaEquipoRival') }}  <strong>{{$data["temperaturaLocalEquipoContrario"]}} {{ __('Final.Grados') }}</strong></li>
        <li>{{ __('Final.Respondidas') }} <strong>{{$data["cantidadPreguntasRespondidas"]}} {{ __('Final.Preguntas') }}</strong></li>
        <li>{{ __('Final.Correctas') }} <strong>{{$data["cantidadPreguntasOptimas"]}} {{ __('Final.Preguntas') }}</strong></li>
        <li>{{ __('Final.Generados') }} <strong>{{$data["cantidadGradosGenerados"]}} {{ __('Final.Grados') }}</strong> </li>
    </ul>
    <h2>{{ __('Final.AccionesErroneas') }}</h2>
        <table class="tabla-estadisticas">
            <thead>
                <tr>
                    <th>{{ __('Final.AccionRealizada') }}</th>
                    <th>{{ __('Final.CantidadJugadores') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data['respuestasIncorrectas'] as $elemento => $cantidad)
                    <tr>
                            <td>{{ $elemento }}</td>
                            <td>{{ $cantidad }} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a class="boton"href="/salas">{{ __('Final.Volver_menu') }}</a>
@endsection
