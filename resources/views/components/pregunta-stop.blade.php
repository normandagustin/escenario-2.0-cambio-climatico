<section id="section-stop" class="ocultar">
    <h3>{{__("Stop.TituloPopup")}}</h3>
    <h4 id="enunciado"></h4>
    <form id="form-stop-respuestas">
        @csrf
        <input id="boton_stop_respuestas" class="boton" type="button" value="Responder">
    </form>
</section>