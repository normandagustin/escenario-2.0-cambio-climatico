{{-- Pop-up de configuracion para el sonido --}}
{{-- Se puede utilizar en todas las pantallas, excepto la del video de presentacion --}}
<section class="section-Pop-up pop_up_hid sound_config">
    <h2>{{ __('Salas.ConfiguracionSonido') }}</h2>
    <label for="sound-slider">{{ __('Salas.Sonido') }}</label>
    <input type="range" id="sound-slider" min="0" max="100" value="100">

    <fieldset>
        <button type="button" class="boton" id="boton_cerrar_sound">{{ __('Salas.Cerrar') }}</button>
    </fieldset>

</section>
