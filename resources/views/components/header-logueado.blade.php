{{-- Componente ---> Header para cuando el usuario se encuentra logueado --}}

<header class="cartel_bienvenido">
    <span class="mensajeAlUsuario arriba">{{__('Salas.Bienvenida')}} {{ auth()->user()->name }}!</span>
    <button type="submit" form="form_log" class="boton botonHeader" id="logout">{{ __('Salas.Desconectarse') }}</button>
    <form method="POST" action="/logout" id="form_log">
        @csrf
    </form>
    @if (auth()->user()->email == 'famawilliams@gmail.com')
        <a href="/admin/preguntas/create" class="boton botonHeader" id="cargarPreguntas">{{ __('Salas.CargarPreguntas') }}</a>
    @endif
    
</header>