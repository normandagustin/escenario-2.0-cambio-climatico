<section id="cargaPreguntaSection" class="hidden">

    <h3>{{ __('Stop.Titulo') }}</h3>
    <fieldset id="form_pregunta">
        <fieldset id="stop_pregunta">
            <label for="input_pregunta">{{ __('Stop.Pregunta') }}</label>
            <input type="text" id="input_pregunta" name="pregunta">
        </fieldset>
        <fieldset id="section_opciones">
            <p>{{ __('Stop.Mensaje') }}</p>
            <fieldset id="opcion_1" class="opcion">
                <label for="input_opcion_1">{{ __('Stop.Opcion') }} 1:</label>
                <input type="text" id="input_opcion_1" name="opcion1" class="input_opcion">
                <input type="radio" name="isOptima" id="radio_opcion_1" class="radio_optima">
            </fieldset>
            <fieldset id="opcion_2" class="opcion">
                <label for="input_opcion_2">{{ __('Stop.Opcion') }} 2:</label>
                <input type="text" id="input_opcion_2" name="opcion2" class="input_opcion">
                <input type="radio" name="isOptima" id="radio_opcion_2" class="radio_optima">
            </fieldset>
            <fieldset id="section_agregar_opcion">
                <input type="button" id="boton_agregar_opcion" value={{ __('Stop.Agregar') }} class="boton">
            </fieldset>
        </fieldset>
        <fieldset id="stop_justi">
            {{-- input de justificacion, un textBox --}}
            <label for="input_justificacion">{{ __('Stop.Justificacion') }}</label>
            <textarea name="justificación" id="input_justificacion" cols="30" rows="5"></textarea>
        </fieldset>
        <fieldset id="stop_minutos">
            {{-- input para tiempo, es number, transofmra a minutos --}}
            <label for="input_minutos">{{ __('Stop.Minutos') }}</label>
            <input type="number" id="input_minutos" min="1">
        </fieldset>
        <fieldset id="stop_grados">
            {{-- Input para poner cantidad de grados a subir o bajar --}}
            <label for="input_grados">{{ __('Stop.Grados') }}</label>
            {{-- //TODO ajustar bien la cantidad o formato de grados, no siempre seran numeros enteros --}}
            <input type="number" id="input_grados" min="1">
        </fieldset>
    </fieldset>
    <fieldset id="section_botones">
        <button id="cancelar_pregunta" class="boton">{{ __('Stop.Cancelar') }}</button>
        <button id="lanzar_pregunta" class="boton">{{ __('Stop.Lanzar') }}</button>
    </fieldset>
</section>
