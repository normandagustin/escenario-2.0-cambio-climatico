class Aplication {
    pantalla_inactiva;
    tiempo_temp;
    jugEqu_temp;
    diffic_temp;
    ref_sala;
    cuerpo;
    self;

    constructor() {
        document.addEventListener("DOMContentLoaded", () => {
            self = this;

            // Rueda de Configuración
            let config = document.querySelector(".config_icon");
            // pop-up Configuración de Sala
            let config_pop = document.querySelector(".config_sala");

            // Cuerpo del documento
            this.cuerpo = document.querySelector(".main_body");

            // boton eliminar sala
            let boton_eliminar_sala = document.getElementById("eliminar_sala");

            // Refrescamos todo lo que tenga que ver con la sala.
            this.ref_sala = setInterval(this.refresh_sala, 2000);

            let mensajeRotarPantalla = document.querySelector(".rotar-pantalla");
            
            this.pantalla_inactiva = document.getElementById("pantalla-inactiva");

            // Salir de sala (solo si es usuario comun solo borrarlo del equipo)
            document
                .getElementById("salir_sala")
                .addEventListener("click", () => {
                    this.salir_sala();
                });

            //Si el dispositivo está en modo horizontal muestramos el mensaje
            if (matchMedia("all and (orientation:landscape)").matches)
                mensajeRotarPantalla.classList.add("no-mostrar");

            //Si el dispositivo cambia la orientación mostramos el mensaje si no es horizontal
            window.addEventListener("orientationchange", function (event) {
                if (event.target.screen.orientation.angle == 90) {
                    mensajeRotarPantalla.classList.add("no-mostrar");
                } else mensajeRotarPantalla.classList.remove("no-mostrar");
            });

            if (boton_eliminar_sala) {
                // Borrar la sala (solo si es anfitrion).
                boton_eliminar_sala.addEventListener("click", () => {
                    this.eliminar_sala();
                });
            }

            if (config) {
                config.addEventListener("click", () => {
                    config_pop.classList.remove("pop_up_hid");
                    //Uso los valores de los labels, para cargar los input de configuración.
                    this.cargar_valores_input_configuracion();
                    this.deshabilitar_cuerpo();
                });
            }

            // Boton cancelar, Cerrar config Sala
            document
                .getElementById("config_cancelar")
                .addEventListener("click", () => {
                    config_pop.classList.add("pop_up_hid");
                    this.habilitar_cuerpo();
                });

            // Boton Guardar
            document
                .getElementById("config_guardar")
                .addEventListener("click", () => {
                    var tiempo =
                        document.getElementById("config_TiempoJuego").value;

                    var jugadores = document.getElementById(
                        "config_NumJugadores"
                    ).value;

                    var dificultad;

                    let radio_dif = document.getElementsByName("dificultad");
                    for (let i = 0; i < radio_dif.length; i++) {
                        if (radio_dif[i].checked)
                            dificultad = radio_dif[i].value;
                    }

                    this.actualizar_sala(tiempo, jugadores, dificultad);
                    config_pop.classList.add("pop_up_hid");
                    this.habilitar_cuerpo();
                });

            config_pop.addEventListener("change", () => {
                // Primero, por cada cambio guarda los valores actuales
                // Valor del Tiempo
                let nuevo_tiempo =
                    document.getElementById("config_TiempoJuego").value;
                if (nuevo_tiempo) {
                    this.tiempo_temp = nuevo_tiempo;
                }

                // Valor de Cant de jugadores
                let nuevo_jug = document.getElementById(
                    "config_NumJugadores"
                ).value;
                if (nuevo_jug) {
                    this.jugEqu_temp = nuevo_jug;
                }

                // Valor de Dificultad
                let radio_dif = document.getElementsByName("dificultad");
                for (let i = 0; i < radio_dif.length; i++) {
                    if (radio_dif[i].checked)
                        this.diffic_temp =
                            radio_dif[i].nextElementSibling.innerHTML;
                }
            });
        }); //cierra el DOMcontentLoader
    }

    deshabilitar_cuerpo() {
        this.cuerpo.classList.add("disabled");
        this.pantalla_inactiva.classList.remove("ocultar");
    }

    habilitar_cuerpo() {
        this.cuerpo.classList.remove("disabled");
        this.pantalla_inactiva.classList.add("ocultar");
    }

    /*   // .getElementsByName("csrf-token")[0]
           /// .getAttribute("content");*/

    /* */
    /* Funcion utilizada para cargar en la ruedita de configuracion, los valores correctos en los input.*/
    /* */

    cargar_valores_input_configuracion() {
        document.getElementById("config_TiempoJuego").value = this.tiempoJuego;

        document.getElementById("config_NumJugadores").value =
            this.numJugadores;

        for (var i = 1; i <= 3; i++) {
            var label = document.getElementById("label_D_" + i);
            var input = document.getElementById("radio_D_" + i);
            if (this.dificultad == label.innerText) {
                input.checked = true;
                break;
            }
        }
    }

    /* */
    /* Comienzo bloque de metodos relacionados con actualizar la sala con datos que vienen el backend */
    /* */

    refresh_sala() {
        fetch("/refrescarSala/")
            .then((response) => response.json())
            .then((data) => {
                self.refrescarContinuidadSala(data.continuidad);
                self.refrescarConfig(data.config);
                self.refrescarEquipos(data.equipos);
                self.refrescarVista(data.estado);
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error.message
                );
            });
    }

    refrescarContinuidadSala(continuidad) {
        if (!continuidad) window.location.href = "/salas";
    }

    refrescarConfig(sala) {
        if (sala) {
            this.tiempoJuego = sala.tiempoJuego;
            this.numJugadores = sala.numJugadores;
            this.dificultad = sala.dificultad;
            //Actualizar la vista.
            let label = document
                .querySelector(".tiempo_juego")
                .innerHTML.split(":")[0];
            document.querySelector(".tiempo_juego").innerHTML =
                label + ": " + sala.tiempoJuego;

            label = document
                .querySelector(".cant_jugadores")
                .innerHTML.split(":")[0];
            document.querySelector(".cant_jugadores").innerHTML =
                label + ": " + sala.numJugadores;

            label = document
                .querySelector(".dificultad")
                .innerHTML.split(":")[0];
            document.querySelector(".dificultad").innerHTML =
                label + ": " + sala.dificultad;
        }
    }

    // Esta funciona redirecciona al usuario segun el estado del juego.
    refrescarVista(estado) {
        if ("jugando" == estado) window.location.href = "/gameplay";
    }

    /* */
    /* Comienzo de bloque de metodos relaciondos con mostar jugadores en la sala, cambio de equipo, kickeo de jugador. */
    /* */

    refrescarEquipos(data) {
        this.limpiar_y_agregar_header_equipos(
            data["equipos"]["jugadores_equipo_1"].length,
            data.equipos.jugadores_totales,
            document.getElementById("section_equipo1")
        );

        this.limpiar_y_agregar_header_equipos(
            data["equipos"]["jugadores_equipo_2"].length,
            data.equipos.jugadores_totales,
            document.getElementById("section_equipo2")
        );

        this.armarListaEquipo(
            data["equipos"]["equipo_1"],
            data["equipos"]["jugadores_equipo_1"],
            data["is_anfi"],
            "izquierda",
            document.getElementById("section_equipo1")
        );

        this.armarListaEquipo(
            data["equipos"]["equipo_2"],
            data["equipos"]["jugadores_equipo_2"],
            data["is_anfi"],
            "derecha",
            document.getElementById("section_equipo2")
        );
    }

    limpiar_y_agregar_header_equipos(
        cantidad_jugadores,
        cantidad_total,
        section
    ) {
        let h3 = section.querySelector("h3");

        let teamWord = h3.innerHTML.split("(")[0];

        h3.innerHTML =
            teamWord + "(" + cantidad_jugadores + "/" + cantidad_total + ")";
    }

    armarListaEquipo(equipo, jugadores, es_anfitrion, orientacion, section) {
        let lista = section.querySelector("ul");
        lista.innerHTML = "";

        jugadores.forEach((jugador) => {
            let itemJugador = PAW.nuevoElemento("li", "", {
                class: "item_user",
            });

            this.crearElementosJugador(jugador, equipo, itemJugador);

            if (es_anfitrion)
                this.agregarElementosAnfitrion(
                    jugador,
                    orientacion,
                    itemJugador
                );

            lista.appendChild(itemJugador);
        });

        section.appendChild(lista);
    }

    crearElementosJugador(jugador, equipo, itemJugador) {
        let inputJugador = PAW.nuevoElemento("input", "", {
            type: "hidden",
            name: "equipo[" + equipo.id + "][]",
            class: "input_hid_user",
            value: jugador.id,
        });

        let nombre_Jugador = PAW.nuevoElemento("p", jugador.name, {
            class: "nombre_jugador",
        });

        itemJugador.appendChild(inputJugador);
        itemJugador.appendChild(nombre_Jugador);
    }

    agregarElementosAnfitrion(jugador, orientacion, itemJugador) {
        let move_button = PAW.nuevoElemento("span", "", { class: "move_icon" });
        move_button.addEventListener("click", () => {
            self.cambioEquipo(jugador.id);
        });

        if (orientacion == "izquierda")
            move_button.classList.add("move_icon_1");
        else move_button.classList.add("move_icon_2");

        itemJugador.appendChild(move_button);

        let icon_kick = PAW.nuevoElemento("span", "", {
            class: "kick_icon",
        });

        icon_kick.addEventListener("click", () => {
            self.kickearJugador(jugador.id);
        });

        itemJugador.appendChild(icon_kick);
    }

    cambioEquipo(id_user) {
        let body = JSON.stringify({
            id_user: id_user,
        });

        var myRequest = PAW.armarRequest(
            "moverJugador",
            body,
            "csrf-token",
            "content"
        );

        fetch(myRequest)
            .then((response) => response.json())
            .then((data) => {
                if (!data) console.log("Error en cambio de equipo");
            })
            .catch(function (error) {
                console.log("Hubo un problema con la petición Fetch:" + error);
            });
    }

    kickearJugador(id_user) {
        var myRequest = PAW.armarRequest(
            "kickearJugador",
            '{"id_user":"' + id_user + '"}',
            "csrf-token",
            "content"
        );

        fetch(myRequest).catch(function (error) {
            console.log(
                "Hubo un problema con la petición Fetch:" + error.message
            );
        });
    }

    /* */
    /* Fin de bloque de metodos relaciondos con mostar jugadores en la sala, cambio de equipo, kickeo de jugador. */
    /* */

    /* */
    /* Fin bloque de metodos relacionados con actualizar la sala con datos que vienen el backend*/
    /* */

    actualizar_sala(tiempo, jugadores, dificultad) {
        var body = JSON.stringify({
            tiempo: tiempo,
            jugadores: jugadores,
            dificultad: dificultad,
        });

        var myRequest = PAW.armarRequest(
            "actualizarSala",
            body,
            "csrf-token",
            "content"
        );

        fetch(myRequest)
            .then((response) => response.json())
            .then((response) => {})
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error.message
                );
            });
    }

    // Salir de la sala (por backend se valida si es anfitrion o no)
    salir_sala() {
        var myRequest = PAW.armarRequest(
            "salirSala",
            "{}",
            "csrf-token",
            "content"
        );

        fetch(myRequest)
            .then((response) => {
                if (response.ok) window.location.href = "/salas";
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error.message
                );
            });
    }

    mostrarMensajeRotarPantalla(){
        if ( (matchMedia('all and (orientation:landscape)').matches) ) {
            this.mensajeRotarPantalla.classList.add("ocultar");
            //clearInterval(this.intervaloMensajeRotarPantalla);
        }
    }

    // Eliminar sala cuando anfitrion toque el boton "eliminar sala".
    eliminar_sala() {
        var myRequest = PAW.armarRequest(
            "/eliminarSala",
            "{}",
            "csrf-token",
            "content"
        );

        fetch(myRequest)
            .then((response) => {
                if (response.ok) {
                    window.location.href = "/salas";
                }
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error.message
                );
            });
    }

}

let App = new Aplication();
