<?php

namespace Database\Factories;

use App\Models\Respuesta;
use App\Models\Pregunta;

use Illuminate\Database\Eloquent\Factories\Factory;

class RespuestaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Respuesta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'texto' => $this->faker->word,
            'grados' => rand(1, 10),
            'isOptima' => $this->faker->boolean,
            'pregunta_id' => Pregunta::Factory()
        ];
    }
}
