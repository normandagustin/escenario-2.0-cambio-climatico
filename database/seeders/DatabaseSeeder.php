<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User;
use \App\Models\Pregunta;
use \App\Models\Respuesta;
use \App\Models\Stop;
use \App\Models\StopRespuesta;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $gradosOptima = 1;
        $gradosIncorrecta = 3;
        //Usuario administrador
        User::factory()->create([
            'name' => 'admin',
            'password' => 'FacuSebaAngel2021',
            'email' => 'famawilliams@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'qwe',
            'password' => 'qweqwe',
            'email' => 'qwe@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'asd',
            'password' => 'asdasd',
            'email' => 'asd@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'Agus',
            'password' => 'agusagus',
            'email' => 'agus@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'Facu',
            'password' => 'facufacu',
            'email' => 'facu@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'Angel',
            'password' => 'angelangel',
            'email' => 'angel@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'Seba',
            'password' => 'angelangel',
            'email' => 'angel@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'Pablo',
            'password' => 'pablopablo',
            'email' => 'pablo@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'Tomas',
            'password' => 'tomastomas',
            'email' => 'tomas@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'Javier',
            'password' => 'javierjavier',
            'email' => 'javier@gmail.com'
        ]);

        User::factory()->create([
            'name' => 'Santiago',
            'password' => 'santiagosantiago',
            'email' => 'santiago@gmail.com'
        ]);

        //
        //Esto es para correrlo y que le pegue a la API para obtener todos los resultados en ingles.
        //No se debe correr seguido porque nos van a bloquear por DAILY LIMIT
        /*$request = [ "idioma" => "es", "elemento_asociado" =>"cepillo", "enunciado" => "Me levanto tempranito, y mi primera rutina diaria es lavarme los dientes. ¿Qué tipo de cepillo de dientes utilizo?", "justificacion" => "Al elegir un cepillo de plástico, estoy contribuyendo a que se sigan produciendo excesivamente productos plásticos (derivados del petróleo) y que al convertirse en basura plástica aumenta la emisión de gases de efecto invernadero, como el metano y etileno (CH4 y C2H4) vs. elegir cepillo de bambú, proviene de un recurso renovable y al final de su vida útil se biodegrada en cientos de días.", "etapa" => "maniana", "dificultad" => "facil","respuesta1" => "Un cepillo de dientes de bambú", "grados1" => 1,"respuesta2" => "Un cepillo de dientes de plástico", "grados2" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"canilla", "enunciado" => "Mientras me cepillo los dientes.¿Qué hago con la canilla?", "justificacion" => "Contribuir a un uso consciente del agua es una buena acción. Ya que el acceso a fuentes de agua no es igualitario y los efectos del cambio climático se hacen visibles, sobre todo, en el agua: en forma de sequías, inundaciones o tormentas.", "etapa" => "maniana", "dificultad" => "facil","respuesta1" => "Dejo la canilla abierta", "grados1" => 3,"respuesta2" => "Solo la abro al momento de enjuagar", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"baniarse", "enunciado" => "Me doy una ducha antes de salir ¿Qué elijo?", "justificacion" => "Contribuir a un uso consciente del agua es una buena acción. Ya que el acceso a fuentes de agua no es igualitario y los efectos del cambio climático se hacen visibles, sobre todo, en el agua: en forma de sequías, inundaciones o tormentas.", "etapa" => "maniana", "dificultad" => "facil","respuesta1" => "Ducha corta", "grados1" => 1,"respuesta2" => "Ducha larga", "grados2" => 3,"respuesta3" => "Baño con bañadera llena", "grados3" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"secador", "enunciado" => "Me seco el pelo...", "justificacion" => "Con el uso de artefactos eléctricos estamos consumiendo cada vez más combustibles fósiles. Y la combustión de combustibles fósiles genera emisiones de gases: CO2, CO, entre otros, que contribuyen a generar y potenciar el efecto invernadero.", "etapa" => "maniana", "dificultad" => "facil","respuesta1" => "Naturalmente, con el viento", "grados1" => 1,"respuesta2" => "Uso secador de pelo, consumo energía eléctrica", "grados2" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"termotanque", "enunciado" => "En casa tengo termotanque...", "justificacion" => "El solar tiene un sistema de tubos colectores que recogen de manera eficiente los rayos solares y transfieren el calor al agua que circula por ellos. El de gas funciona por combustión de hidrocarburos y, hace aumentar las emisiones de gases de efecto invernadero.El eléctrico aumenta el consumo de energía eléctrica.", "etapa" => "maniana", "dificultad" => "facil","respuesta1" => "Solar", "grados1" => 1,"respuesta2" => "A gas", "grados2" => 3,"respuesta3" => "Eléctrico", "grados3" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"puertaCasa", "enunciado" => "Voy a ir a la escuela ¿Cómo voy?", "justificacion" => "Los medios de transporte como colectivos o autos consumen combustibles fósiles ( hidrocarburos derivados del petróleo) En el transporte público el consumo se distribuye entre más personas.", "etapa" => "maniana", "dificultad" => "facil","respuesta1" => "Caminando", "grados1" => 1,"respuesta2" => "En auto", "grados2" => 3,"respuesta3" => "En transporte público", "grados3" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"almuerzo", "enunciado" => "Se acerca la hora del almuerzo, y en el comedor del colegio hay verduras ¿Compras una tarta de verduras o haces una ensalada?", "justificacion" => "Elegir consumir vegetales limpios crudos, así no consumo Energía", "etapa" => "mediodia", "dificultad" => "facil","respuesta1" => "Tarta de verduras", "grados1" => 3,"respuesta2" => "Ensalada", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"cafe", "enunciado" => "Quiero tomar un café en la cafetería ¿Uso vasito de plástico, cuchara de plástico o llevo mi vaso de casa?", "justificacion" => "No utilizar materiales plásticos desechables, están hechos de polímeros sintéticos derivados del petróleo, se usan una vez y aumentan el volumen de residuos sólidos. Se biodegradan muy muy lentamente.", "etapa" => "mediodia", "dificultad" => "facil","respuesta1" => "Uso material plástico desechable", "grados1" => 3,"respuesta2" => "Reuso mi taza", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"hojas", "enunciado" => "Tengo que comprar hojas para la carpeta…", "justificacion" => "Elegir comprar productos locales ( mientras más lejos venga el producto se utiliza más transporte, más consumo de combustibles). Las hojas de caña de azúcar son más sustentables que las de fibra de árbol", "etapa" => "mediodia", "dificultad" => "facil","respuesta1" => "voy a la librería cercana y compro hojas hechas con caña de azúcar", "grados1" => 1,"respuesta2" => "voy a la librería cercana y compro hojas hechas con fibra de árboles", "grados2" => 3,"respuesta3" => "compro por delivery hojas de una librería lejana", "grados3" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"estufa", "enunciado" => "Está empezando a hacer frío y prendo la estufa…", "justificacion" => "Verificar el color de la llama en cuanto a la combustión completa e incompleta. La combustión incompleta es menos energética y tiene un rendimiento menor. Además está el peligro de la intoxicación por monóxido de carbono", "etapa" => "mediodia", "dificultad" => "facil","respuesta1" => "la llama de la combustión de la estufa es amarilla y la dejo asi", "grados1" => 3,"respuesta2" => "la llama de la combustión del la estufa es azul y la dejo asi", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"luz", "enunciado" => "Tengo que estudiar y hacer las tareas del colegio.", "justificacion" => "Usar la luz natural es muy positivo porque se reduce el consumo de electricidad, bajan los costos de la factura mensual y tu cerebro entiende que es de día activando así tu energía y buen humor para predisponerte a hacer las labores.", "etapa" => "mediodia", "dificultad" => "facil","respuesta1" => "Abro las ventanas y aprovecho la luz natural", "grados1" => 1,"respuesta2" => "Me encierro y prendo el ventilador", "grados2" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"aireFresco", "enunciado" => "Necesito fresco en verano…", "justificacion" => "Las sombrillas son de metal y plásticos , al plantar un árbol mediante la fotosíntesis genero oxígeno y consumo CO2 de la atmósfera.", "etapa" => "mediodia", "dificultad" => "facil","respuesta1" => "compro una sombrilla/pergola/media sombra", "grados1" => 3,"respuesta2" => "planto un arbol", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"consola", "enunciado" => "Quiero jugar en la consola pero necesito irme un rato para chatear con mi amiga...", "justificacion" => "La energía eléctrica utilizada proviene de fuentes renovables y no renovables como la quema de gases y carbón: Contribuye entonces a la emisión de GEIs indirectamente. Mantenerlos apagados cuando no se utilizan evita la emisión de GEIs", "etapa" => "mediodia", "dificultad" => "facil","respuesta1" => "dejo la consola encendida", "grados1" => 3,"respuesta2" => "guardo el progreso y la apago un rato", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"regalo", "enunciado" => "Tengo que hacer un regalo a un amigo que se va de viaje", "justificacion" => "Elegir consumir productos elaborados en la zona disminuye la emisión de GEIs, generados por transporte. Así mismo, elegir productos que no incluyan plástico en su envase también es un hábito amigable con el medio ambiente.", "etapa" => "mediodia", "dificultad" => "facil","respuesta1" => "Le compro una agenda viajera de papel orgánico producida en China", "grados1" => 3,"respuesta2" => "Elijo regalarle un producto de higiene elaborado por un artesano local. (Shampoo sólido)", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"cena", "enunciado" => "Se acerca la hora de la cena, y en tu heladera hay acelga, huevos, harina. ¿Cocinas una tarta de verduras o pedís una hamburguesa en el bar más cercano?", "justificacion" => "Al elegir consumir carne , aumenta la emisión de CH4 proveniente de la ganadería vs consumir vegetales. El delivery aumenta las emisiones por el transporte y uso de combustibles.", "etapa" => "tarde", "dificultad" => "facil","respuesta1" => "Tarta de acelga", "grados1" => 1,"respuesta2" => "Hamburguesa", "grados2" => 3,"respuesta3" => "Delivery de hamburguesa", "grados3" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"cena2", "enunciado" => "Se acerca la hora de la cena, prendo el horno y... ¿Cocino mi cena o cocino mi cena y aprovecho para cocinar el almuerzo del día siguiente al mismo tiempo?", "justificacion" => "Elegir optimizar las cocciones simultáneas, maximizando el uso del horno y minimizando el combustible usado", "etapa" => "tarde", "dificultad" => "facil","respuesta1" => "Solo la cena", "grados1" => 3,"respuesta2" => "La cena y mi almuerzo del dia siguiente", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"basura", "enunciado" => "Se acerca la hora de dormir y limpiamos un poco la casa..¿Separás los residuos de tu hogar en orgánicos, inorgánicos?", "justificacion" => "Separar los residuos orgánicos para su compostaje, disminuyendo así la basura que recoge el camión recolector. Menos masa, menos combustible usado en transporte.", "etapa" => "tarde", "dificultad" => "facil","respuesta1" => "Sí hago compost", "grados1" => 1,"respuesta2" => "No, todo a la misma bolsa", "grados2" => 3,"respuesta3" => "Quemar la basura", "grados3" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"helado", "enunciado" => "Después de cenar te dieron ganas de un helado. ¿Vas en bicicleta a la heladería más cercana o llamas al delivery?", "justificacion" => "Caminar y andar en bicicleta son actividades físicas que no solo liberan endorfinas, hormonas que te hacen sentir más feliz, sino que al mismo tiempo se reducen las emisiones de GEIs del transporte del delivery.", "etapa" => "tarde", "dificultad" => "facil","respuesta1" => "Delivery", "grados1" => 3,"respuesta2" => "Caminata nocturna", "grados2" => 1,"respuesta3" => "Hago el trayecto en bicicleta", "grados3" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"SecarRopa", "enunciado" => "Mi cesto de ropa sucia se llenó, le digo a mamá que…", "justificacion" => "Los calefactores a gas, funcionan quemando metano y liberando CO2 al ambiente, contribuyendo así a que eleve su concentración y al cambio climático. Optar por la luz natural siempre es la mejor opción.", "etapa" => "tarde", "dificultad" => "facil","respuesta1" => "Lave la ropa y prendo el calefactor para tenderla y que se seque durante la noche.", "grados1" => 3,"respuesta2" => "Lave la ropa durante la noche y la tiendo a la mañana siguiente, aprovechando la luz solar para su secado.", "grados2" => 1,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"cumple", "enunciado" => "Organizamos un cumple con mis compañeros/as, y generamos mucha basura", "justificacion" => "Separar los residuos, ayuda a reducir la basura que recoge el camión recolector. Y además estamos colaborando o bien con recicladores urbanos, o con cooperativa de reciclaje, y fundación botella de amor.", "etapa" => "tarde", "dificultad" => "facil","respuesta1" => "Separamos los residuos", "grados1" => 1,"respuesta2" => "Armamos botellas de amor con todos los envoltorios plásticos", "grados2" => 3,"respuesta3" => "Tiramos todo", "grados3" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"basuraVecinos", "enunciado" => "En el barrio cada vez se acumulan más bolsas de basura de los vecinos...¿Qué puedo hacer al respecto?", "justificacion" => "Separar los residuos en húmedos y secos, es el primer paso para reducir la cantidad de bolsas de basura que sacamos a la calle.", "etapa" => "tarde", "dificultad" => "facil","respuesta1" => "Comunicarles la importancia de la separación de residuos en húmedos y secos", "grados1" => 3,"respuesta2" => "Hacer un compost comunitario en la cuadra", "grados2" => 1,"respuesta3" => "No hacer nada, si total nada va a cambiar", "grados3" => 3,];
        Pregunta::crear($request);
        $request = [ "idioma" => "es", "elemento_asociado" =>"cambioDeRopa", "enunciado" => "Nunca me es suficiente la ropa que tengo, me aburre y quiero estar a la moda ¿Qué hago?", "justificacion" => "El fast fashion provoca que se introduzcan al mercado muchas colecciones de ropa “en tendencia”, durante lapsos breves. Ropa que se usa poco y nada, se genera consumo innecesario y desmedido y producciones en masa. La industria de la moda genera una gran cantidad de gases de efecto invernadero (CO2) debido a la energía que emplea en los procesos de producción, fabricación y transporte de millones de prendas.", "etapa" => "tarde", "dificultad" => "facil","respuesta1" => "Intercambio con alguien de mi talla, lo que ya no uso por algo que me guste más", "grados1" => 1,"respuesta2" => "Vendo en alguna tienda de ropa usada y asi me compro algo de segunda mano también", "grados2" => 3,"respuesta3" => "Me compro nueva, total es barata", "grados3" => 3,];
        Pregunta::crear($request);*/
        ///////

        $pregunta = Pregunta::create([ "elemento_asociado" => "cepillo", "enunciado" => "I get up early, and my first daily routine is to brush my teeth. What kind of toothbrush do I use?", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "By choosing a plastic brush, I am contributing to the excessive production of plastic products (derived from oil) and that, by becoming plastic garbage, increases the emission of greenhouse gases, such as methane and ethylene (CH4 and C2H4) vs. choose bamboo brush, it comes from a renewable resource and at the end of its useful life it biodegrades in hundreds of days.", "language" => "en" ]);
        Respuesta::create([ "texto" => "A bamboo toothbrush", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "A plastic toothbrush", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cepillo", "enunciado" => "Me levanto tempranito, y mi primera rutina diaria es lavarme los dientes. ¿Qué tipo de cepillo de dientes utilizo?", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "Al elegir un cepillo de plástico, estoy contribuyendo a que se sigan produciendo excesivamente productos plásticos (derivados del petróleo) y que al convertirse en basura plástica aumenta la emisión de gases de efecto invernadero, como el metano y etileno (CH4 y C2H4) vs. elegir cepillo de bambú, proviene de un recurso renovable y al final de su vida útil se biodegrada en cientos de días.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Un cepillo de dientes de bambú", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Un cepillo de dientes de plástico", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "canilla", "enunciado" => "While I brush my teeth, what do I do with the tap?", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "Contributing to a conscious use of water is a good deed. Since access to water sources is not equal and the effects of climate change are visible, above all, in water: in the form of droughts, floods or storms.", "language" => "en" ]);
        Respuesta::create([ "texto" => "I leave the faucet open", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "I only open it when rinsing", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "canilla", "enunciado" => "Mientras me cepillo los dientes.¿Qué hago con la canilla?", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "Contribuir a un uso consciente del agua es una buena acción. Ya que el acceso a fuentes de agua no es igualitario y los efectos del cambio climático se hacen visibles, sobre todo, en el agua: en forma de sequías, inundaciones o tormentas.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Dejo la canilla abierta", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Solo la abro al momento de enjuagar", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "baniarse", "enunciado" => "I take a shower before going out. What do I choose?", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "Contributing to a conscious use of water is a good deed. Since access to water sources is not equal and the effects of climate change are visible, above all, in water: in the form of droughts, floods or storms.", "language" => "en" ]);
        Respuesta::create([ "texto" => "short shower", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "long shower", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Bathroom with full bathtub", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "baniarse", "enunciado" => "Me doy una ducha antes de salir ¿Qué elijo?", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "Contribuir a un uso consciente del agua es una buena acción. Ya que el acceso a fuentes de agua no es igualitario y los efectos del cambio climático se hacen visibles, sobre todo, en el agua: en forma de sequías, inundaciones o tormentas.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Ducha corta", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Ducha larga", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Baño con bañadera llena", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "secador", "enunciado" => "I dry my hair...", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "With the use of electrical devices we are consuming more and more fossil fuels. And the combustion of fossil fuels generates gas emissions: CO2, CO, among others, which contribute to generating and enhancing the greenhouse effect.", "language" => "en" ]);
        Respuesta::create([ "texto" => "Of course with the wind", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Hair dryer use, electricity consumption", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "secador", "enunciado" => "Me seco el pelo...", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "Con el uso de artefactos eléctricos estamos consumiendo cada vez más combustibles fósiles. Y la combustión de combustibles fósiles genera emisiones de gases: CO2, CO, entre otros, que contribuyen a generar y potenciar el efecto invernadero.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Naturalmente, con el viento", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Uso secador de pelo, consumo energía eléctrica", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "termotanque", "enunciado" => "At home I have a hot water tank...", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "The solar has a system of collector tubes that efficiently collect the sun's rays and transfer the heat to the water that circulates through them. The gas one works by combustion of hydrocarbons and increases greenhouse gas emissions. The electric one increases the consumption of electrical energy.", "language" => "en" ]);
        Respuesta::create([ "texto" => "Solar", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "gas", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Electric", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "termotanque", "enunciado" => "En casa tengo termotanque...", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "El solar tiene un sistema de tubos colectores que recogen de manera eficiente los rayos solares y transfieren el calor al agua que circula por ellos. El de gas funciona por combustión de hidrocarburos y, hace aumentar las emisiones de gases de efecto invernadero.El eléctrico aumenta el consumo de energía eléctrica.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Solar", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "A gas", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Eléctrico", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "puertaCasa", "enunciado" => "I'm going to school. How am I going?", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "Means of transport such as buses or cars consume fossil fuels (oil-derived hydrocarbons). In public transport, consumption is distributed among more people.", "language" => "en" ]);
        Respuesta::create([ "texto" => "Walking", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "By car", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "By public transport", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "puertaCasa", "enunciado" => "Voy a ir a la escuela ¿Cómo voy?", "dificultad" => "facil", "etapa" => "maniana", "justificacion" => "Los medios de transporte como colectivos o autos consumen combustibles fósiles ( hidrocarburos derivados del petróleo) En el transporte público el consumo se distribuye entre más personas.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Caminando", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "En auto", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "En transporte público", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "almuerzo", "enunciado" => "Lunch time is approaching, and there are vegetables in the school canteen. Do you buy a vegetable pie or do you make a salad?", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Choosing to consume clean raw vegetables, so I do not consume Energy", "language" => "en" ]);
        Respuesta::create([ "texto" => "Vegetable Tart", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Salad", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "almuerzo", "enunciado" => "Se acerca la hora del almuerzo, y en el comedor del colegio hay verduras ¿Compras una tarta de verduras o haces una ensalada?", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Elegir consumir vegetales limpios crudos, así no consumo Energía", "language" => "es" ]);
        Respuesta::create([ "texto" => "Tarta de verduras", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Ensalada", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cafe", "enunciado" => "I want to have a coffee in the cafeteria. Do I use a plastic cup, a plastic spoon or do I bring my glass from home?", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Do not use disposable plastic materials, they are made of synthetic polymers derived from petroleum, they are used once and increase the volume of solid waste. They biodegrade very very slowly.", "language" => "en" ]);
        Respuesta::create([ "texto" => "Use disposable plastic material", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "I reuse my cup", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cafe", "enunciado" => "Quiero tomar un café en la cafetería ¿Uso vasito de plástico, cuchara de plástico o llevo mi vaso de casa?", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "No utilizar materiales plásticos desechables, están hechos de polímeros sintéticos derivados del petróleo, se usan una vez y aumentan el volumen de residuos sólidos. Se biodegradan muy muy lentamente.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Uso material plástico desechable", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Reuso mi taza", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "hojas", "enunciado" => "I have to buy sheets for the folder…", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Choose to buy local products (the further the product comes, the more transport is used, the more fuel consumption). Sugar cane leaves are more sustainable than tree fiber leaves", "language" => "en" ]);
        Respuesta::create([ "texto" => "I go to the nearby bookstore and buy leaves made with sugar cane", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "I go to the nearby bookstore and buy sheets made from tree fiber", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "I buy for delivery sheets from a distant bookstore", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "hojas", "enunciado" => "Tengo que comprar hojas para la carpeta…", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Elegir comprar productos locales ( mientras más lejos venga el producto se utiliza más transporte, más consumo de combustibles). Las hojas de caña de azúcar son más sustentables que las de fibra de árbol", "language" => "es" ]);
        Respuesta::create([ "texto" => "voy a la librería cercana y compro hojas hechas con caña de azúcar", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "voy a la librería cercana y compro hojas hechas con fibra de árboles", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "compro por delivery hojas de una librería lejana", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "estufa", "enunciado" => "It's starting to get cold and I turn on the stove...", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Check the color of the flame for complete and incomplete combustion. Incomplete combustion is less energetic and has a lower yield. Then there is the danger of carbon monoxide poisoning.", "language" => "en" ]);
        Respuesta::create([ "texto" => "the combustion flame of the stove is yellow and I leave it that way", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "the combustion flame of the stove is blue and I leave it that way", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "estufa", "enunciado" => "Está empezando a hacer frío y prendo la estufa…", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Verificar el color de la llama en cuanto a la combustión completa e incompleta. La combustión incompleta es menos energética y tiene un rendimiento menor. Además está el peligro de la intoxicación por monóxido de carbono", "language" => "es" ]);
        Respuesta::create([ "texto" => "la llama de la combustión de la estufa es amarilla y la dejo asi", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "la llama de la combustión del la estufa es azul y la dejo asi", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "luz", "enunciado" => "I have to study and do my homework.", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Using natural light is very positive because electricity consumption is reduced, the costs of the monthly bill are lowered and your brain understands that it is daytime, thus activating your energy and good mood to predispose you to do the work.", "language" => "en" ]);
        Respuesta::create([ "texto" => "I open the windows and take advantage of the natural light", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "I lock myself in and turn on the fan", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "luz", "enunciado" => "Tengo que estudiar y hacer las tareas del colegio.", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Usar la luz natural es muy positivo porque se reduce el consumo de electricidad, bajan los costos de la factura mensual y tu cerebro entiende que es de día activando así tu energía y buen humor para predisponerte a hacer las labores.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Abro las ventanas y aprovecho la luz natural", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Me encierro y prendo el ventilador", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "aireFresco", "enunciado" => "I need cool in summer...", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "The umbrellas are made of metal and plastic, when planting a tree through photosynthesis I generate oxygen and consume CO2 from the atmosphere.", "language" => "en" ]);
        Respuesta::create([ "texto" => "buy an umbrella/pergola/half shade", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "I plant a tree", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "aireFresco", "enunciado" => "Necesito fresco en verano…", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Las sombrillas son de metal y plásticos , al plantar un árbol mediante la fotosíntesis genero oxígeno y consumo CO2 de la atmósfera.", "language" => "es" ]);
        Respuesta::create([ "texto" => "compro una sombrilla/pergola/media sombra", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "planto un arbol", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "consola", "enunciado" => "I want to play on the console but I need to go away for a while to chat with my friend...", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "The electrical energy used comes from renewable and non-renewable sources such as the burning of gases and coal: it thus contributes to the emission of GHGs indirectly. Keeping them turned off when not in use prevents the emission of GHGs", "language" => "en" ]);
        Respuesta::create([ "texto" => "I leave the console on", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "I save the progress and turn it off for a while", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "consola", "enunciado" => "Quiero jugar en la consola pero necesito irme un rato para chatear con mi amiga...", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "La energía eléctrica utilizada proviene de fuentes renovables y no renovables como la quema de gases y carbón: Contribuye entonces a la emisión de GEIs indirectamente. Mantenerlos apagados cuando no se utilizan evita la emisión de GEIs", "language" => "es" ]);
        Respuesta::create([ "texto" => "dejo la consola encendida", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "guardo el progreso y la apago un rato", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "regalo", "enunciado" => "I have to give a gift to a friend who is going on a trip", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Choosing to consume products made in the area reduces the emission of GHGs, generated by transportation. Likewise, choosing products that do not include plastic in their packaging is also an environmentally friendly habit.", "language" => "en" ]);
        Respuesta::create([ "texto" => "I buy him a travel planner made of organic paper produced in China", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "I choose to give her a hygiene product made by a local artisan. (solid shampoo)", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "regalo", "enunciado" => "Tengo que hacer un regalo a un amigo que se va de viaje", "dificultad" => "facil", "etapa" => "mediodia", "justificacion" => "Elegir consumir productos elaborados en la zona disminuye la emisión de GEIs, generados por transporte. Así mismo, elegir productos que no incluyan plástico en su envase también es un hábito amigable con el medio ambiente.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Le compro una agenda viajera de papel orgánico producida en China", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Elijo regalarle un producto de higiene elaborado por un artesano local. (Shampoo sólido)", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cena", "enunciado" => "Dinner time is approaching, and in your fridge there is chard, eggs, flour. Do you cook a vegetable pie or order a hamburger at the nearest bar?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "By choosing to consume meat, the emission of CH4 from livestock increases vs. consuming vegetables. Delivery increases emissions from transportation and fuel use.", "language" => "en" ]);
        Respuesta::create([ "texto" => "Chard Tart", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Burger", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "hamburger delivery", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cena", "enunciado" => "Se acerca la hora de la cena, y en tu heladera hay acelga, huevos, harina. ¿Cocinas una tarta de verduras o pedís una hamburguesa en el bar más cercano?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Al elegir consumir carne , aumenta la emisión de CH4 proveniente de la ganadería vs consumir vegetales. El delivery aumenta las emisiones por el transporte y uso de combustibles.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Tarta de acelga", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Hamburguesa", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Delivery de hamburguesa", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cena2", "enunciado" => "Dinner time is approaching, I turn on the oven and... Do I cook my dinner or do I cook my dinner and take the opportunity to cook the next day's lunch at the same time?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Choosing to optimize simultaneous cooking, maximizing the use of the oven and minimizing the fuel used", "language" => "en" ]);
        Respuesta::create([ "texto" => "just dinner", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Dinner and my lunch the next day", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cena2", "enunciado" => "Se acerca la hora de la cena, prendo el horno y... ¿Cocino mi cena o cocino mi cena y aprovecho para cocinar el almuerzo del día siguiente al mismo tiempo?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Elegir optimizar las cocciones simultáneas, maximizando el uso del horno y minimizando el combustible usado", "language" => "es" ]);
        Respuesta::create([ "texto" => "Solo la cena", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "La cena y mi almuerzo del dia siguiente", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "basura", "enunciado" => "Bedtime is approaching and we clean the house a bit. Do you separate your household waste into organic, inorganic?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Separate organic waste for composting, thus reducing the garbage collected by the collection truck. Less mass, less fuel used in transportation.", "language" => "en" ]);
        Respuesta::create([ "texto" => "Yes I compost", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "No, all in the same bag", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "burn the garbage", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "basura", "enunciado" => "Se acerca la hora de dormir y limpiamos un poco la casa..¿Separás los residuos de tu hogar en orgánicos, inorgánicos?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Separar los residuos orgánicos para su compostaje, disminuyendo así la basura que recoge el camión recolector. Menos masa, menos combustible usado en transporte.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Sí hago compost", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "No, todo a la misma bolsa", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Quemar la basura", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "helado", "enunciado" => "After dinner you wanted an ice cream. Do you cycle to the nearest ice cream parlor or call delivery?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Walking and cycling are physical activities that not only release endorphins, hormones that make you feel happier, but also reduce GHG emissions from delivery transportation.", "language" => "en" ]);
        Respuesta::create([ "texto" => "Delivery", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Night walk", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "I do the bike ride", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "helado", "enunciado" => "Después de cenar te dieron ganas de un helado. ¿Vas en bicicleta a la heladería más cercana o llamas al delivery?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Caminar y andar en bicicleta son actividades físicas que no solo liberan endorfinas, hormonas que te hacen sentir más feliz, sino que al mismo tiempo se reducen las emisiones de GEIs del transporte del delivery.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Delivery", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Caminata nocturna", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Hago el trayecto en bicicleta", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "SecarRopa", "enunciado" => "My laundry basket is full, I tell mom that…", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Gas heaters work by burning methane and releasing CO2 into the environment, thus contributing to its concentration and climate change. Opting for natural light is always the best option.", "language" => "en" ]);
        Respuesta::create([ "texto" => "I wash my clothes and turn on the heater to hang them up to dry overnight.", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "I wash the clothes overnight and hang them out the next morning, taking advantage of the sunlight to dry them.", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "SecarRopa", "enunciado" => "Mi cesto de ropa sucia se llenó, le digo a mamá que…", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Los calefactores a gas, funcionan quemando metano y liberando CO2 al ambiente, contribuyendo así a que eleve su concentración y al cambio climático. Optar por la luz natural siempre es la mejor opción.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Lave la ropa y prendo el calefactor para tenderla y que se seque durante la noche.", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Lave la ropa durante la noche y la tiendo a la mañana siguiente, aprovechando la luz solar para su secado.", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cumple", "enunciado" => "We organize a birthday with my colleagues, and we generate a lot of garbage", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Separating the waste helps reduce the garbage collected by the garbage truck. And we are also collaborating either with urban recyclers, or with a recycling cooperative, and the bottle of love foundation.", "language" => "en" ]);
        Respuesta::create([ "texto" => "We separate the waste", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "We put together bottles of love with all the plastic wrappers", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "we throw everything", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cumple", "enunciado" => "Organizamos un cumple con mis compañeros/as, y generamos mucha basura", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Separar los residuos, ayuda a reducir la basura que recoge el camión recolector. Y además estamos colaborando o bien con recicladores urbanos, o con cooperativa de reciclaje, y fundación botella de amor.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Separamos los residuos", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Armamos botellas de amor con todos los envoltorios plásticos", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Tiramos todo", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "basuraVecinos", "enunciado" => "In the neighbourhood, more and more garbage bags from the neighbors accumulate... What can I do about it?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Separating the waste into wet and dry is the first step to reduce the amount of garbage bags that we take out on the street.", "language" => "en" ]);
        Respuesta::create([ "texto" => "Communicate the importance of separating waste into wet and dry", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Make a community compost in the block", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Do nothing, if nothing is going to change", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "basuraVecinos", "enunciado" => "En el barrio cada vez se acumulan más bolsas de basura de los vecinos...¿Qué puedo hacer al respecto?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Separar los residuos en húmedos y secos, es el primer paso para reducir la cantidad de bolsas de basura que sacamos a la calle.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Comunicarles la importancia de la separación de residuos en húmedos y secos", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Hacer un compost comunitario en la cuadra", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "No hacer nada, si total nada va a cambiar", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cambioDeRopa", "enunciado" => "The clothes I have are never enough for me, they bore me and I want to be fashionable. What do I do?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "Fast fashion causes many “in-trend” clothing collections to be introduced to the market for short periods of time. Clothes that are used little or nothing, unnecessary and excessive consumption and mass production are generated. The fashion industry generates a large amount of greenhouse gases (CO2) due to the energy it uses in the production, manufacturing and transportation processes of millions of garments.", "language" => "en" ]);
        Respuesta::create([ "texto" => "I exchange with someone of my size, what I no longer use for something that I like more", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "I sell in a used clothing store and that way I buy something second-hand too", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "I buy new, total is cheap", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        $pregunta = Pregunta::create([ "elemento_asociado" => "cambioDeRopa", "enunciado" => "Nunca me es suficiente la ropa que tengo, me aburre y quiero estar a la moda ¿Qué hago?", "dificultad" => "facil", "etapa" => "tarde", "justificacion" => "El fast fashion provoca que se introduzcan al mercado muchas colecciones de ropa “en tendencia”, durante lapsos breves. Ropa que se usa poco y nada, se genera consumo innecesario y desmedido y producciones en masa. La industria de la moda genera una gran cantidad de gases de efecto invernadero (CO2) debido a la energía que emplea en los procesos de producción, fabricación y transporte de millones de prendas.", "language" => "es" ]);
        Respuesta::create([ "texto" => "Intercambio con alguien de mi talla, lo que ya no uso por algo que me guste más", "pregunta_id" => $pregunta->id,"isOptima" => 1, "grados" => "$gradosOptima", ]);
        Respuesta::create([ "texto" => "Vendo en alguna tienda de ropa usada y asi me compro algo de segunda mano también", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        Respuesta::create([ "texto" => "Me compro nueva, total es barata", "pregunta_id" => $pregunta->id,"isOptima" => 0, "grados" => "$gradosIncorrecta", ]);
        

        $stop = new Stop;
        $stop->enunciado = 'Prueba Stop';
        $stop->justificacion = 'Conviene la respuesta X porque X=>Y';
        $stop->minutos = 2;
        $stop->grados = 99;
        $stop->save();

        $stopRespuesta = new StopRespuesta;
        $stopRespuesta->texto = 'Respuesta A';
        $stopRespuesta->isOptima = false;
        $stopRespuesta->stop_id =  $stop->id;
        $stopRespuesta->save();

        $stopRespuesta = new StopRespuesta;
        $stopRespuesta->texto = 'Respuesta B';
        $stopRespuesta->isOptima = true;
        $stopRespuesta->stop_id =  $stop->id;
        $stopRespuesta->save();

        // StopRespuesta::factory()->create([
        //     'texto' => 'Respuesta B',
        //     'votos' => 0,
        //     'isOptima' => true,
        //     "stop_id" => $stop->id,
        // ]);
    }
}
