<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSalaToAccionRealizadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accion_realizadas', function (Blueprint $table) {
            $table->unsignedBigInteger('id_sala');
            $table->foreign('id_sala')->references('id')->on('salas')->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accion_realizadas', function (Blueprint $table) {
            //
        });
    }
}
